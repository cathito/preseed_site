<!DOCTYPE html>

<html lang = "pt-br">

	<header>
		<meta charset="utf-8"/>
		<title> Projeto Login </title>
		<link rel="stylesheet" href="_css/t_login.css"/>
	</header>
	
	
	<body>
		
		<div id="corpo-form">
			<form method="POST" action="classes/login.php">
			
				<h1> Entrar </h1>
				<input type="text" placeholder="Login" name="login" maxlength="12"/>
				<input type="password" placeholder="Senha" name="senha" maxlength="12"/>
				<input type="submit" value="Entrar" id="entrar" name="entrar"><br>
				
				<p> Ainda Não é Inscrito? <a href="cadastrar.php"> <strong> Cadastre-Se </strong> </a> </p>
				<p> <a href="esqueciSenha.php" name="b"> <strong> Esqueceu a senha? </strong> </a> </p>
				
			</form>
		</div>
		
		
	</body>
	
	
</html>
<!DOCTYPE html>

<html lang = "pt-br">

	<head> 
		<meta charset = "UTF-8"/>
		<title> Simula Pre-seed</title>
		<link rel = "stylesheet" href = "_css/t_inicio.css"/>
		<link rel = "stylesheet" href = "_css/faleConosco.css"/>
	</head>
<!--==================================================================-->

	<body>
		<div id="interface"> 
			<header id = "cabecalho">
				<nav id = "menu">
					<h1>Menu Principal </h1>
					
					<ul type = "default" ><!-- start = "default"> para ol-->
						<li> <a href ="	index.php"> Página Inicial </a> </li>
						
						<li> <a href ="login.php"> Login </a> </li>
						
						<li> <a href ="escolhaSimulado.php"> Simulados </a> </li>
						
						<li> <a href ="faleConosco.php"> Fale conosco </a> </li>
						
					</ul>
				</nav>
				
			</header>
		<!--=========================================================-->
			<section id="corpo-full">
				
				<form method="post" id="fContato" action="mailto:cathitojf@hotmail.com"> 
				
					<fieldset id="mensagem"> <legend>&nbsp; Mensagem do Usuário &nbsp;</legend> 
						<p> <label for="cTitulo"> Titulo: </label> <input type="text" name="tTitulo" id="cTitulo" size="58" maxlength="50" placeholder="Assunto..."/> </p> 
						<p> <textarea name="tMsg" id="cMsg" cols="60" rows="8" placeholder="Deixe Aqui Sua Mensagem..."></textarea> </p>
					</fieldset>
					
					<input type="submit" value="Enviar" id="enviar">
					
				</form>
				
			</section>
		<!--========================================================-->
		
			<div id="rodape">
			<footer id="rodape">
				<p> Copyright &copy; 2020 - by João Ferreira <br/>
				<a href="https://www.facebook.com/profile.php?id=100013593917417" target="_blank" id="facebook"> Facebook</a> |
				
				<a href="https://www.instagram.com/joao.ferreira9899" target="_blank" id="instagram"> Instagram </a>
				</p>
				
			</footer>
			</div>
			
		<!--========================================================-->
		
		</div>
	<body>


</html>

<!DOCTYPE html>

<html lang = "pt-br">

	<head> 
		<meta charset = "UTF-8"/>
		<title> Simula Pre-seed</title>
		<link rel = "stylesheet" href = "_css/t_inicio.css"/>
	</head>
<!--==================================================================-->

	<body>
	
		<div id="cabecalho"> 
			<header id = "cabecalho">
				<nav id = "menu">
					<ul type = "default" ><!-- start = "default"> para ol-->
						<li> <a href ="	index.php"> Página Inicial </a> </li>
						
						<li> <a href ="login.php"> Login </a> </li>
						
						<li> <a href ="escolhaSimulado.php"> Simulados </a> </li>
						
						<li> <a href ="faleConosco.php"> Fale conosco </a> </li>
					</ul>
				</nav>
				
				<img id="icone" src = "_imagens/logocp.png"/>
				
			</header>
		</div>
	
<!--=======================================================================================================================================-->
	
		<div> <!--=== principal ===-->
			
			<img id="inicio" src = "_imagens/people-notes-meeting-team-7095.jpg" width="1333" />
			
			<h1 id="recepcao"> SEJAM BEM VINDO(AS)! </h1>
			
			
			<div id="desenvolvimento"> 
			
				<div id="corpo">
					<h1> Exame Nacional do Ensino Médio (ENEM) </h1>
					
					<p> O Exame Nacional do Ensino Médio (Enem) é uma prova realizada anualmente sendo aplicado em dois dias de prova, contendo 180 questões objetivas (divididas em quatro grandes áreas) e uma questão de redação.</p> 
					
					<p> O Exame Nacional do Ensino Médio (Enem) é utilizado para avaliar a qualidade do ensino médio no país. Seu resultado serve para acesso ao ensino superior brasileiro em universidades públicas e particulares. </p>
					
					<p>O acesso ao ensino superior em universidades públicas ocorre através do Sistema de Seleção Unificada (SiSU), em universidades particulars através do Programa Universidade para Todos (ProUni) ou atraves do Fundo de Financiamento ao Estudante do Ensino Superior (Fies). </p>
					
				</div>
				
				
				<div id="noticiario">
					<h1 id="ultimasNoticias"> Últimas Notícias! </h1>
					
					<section id="noticia1"> 
						<h2> Malu - Disco Arranhado </h2>
						
						<p> Quebra a rotina</p> 
						<p> E leva sua mina pra dançar </p>
						<p> Ao som daquela banda cover </p>
						<br/>
						<p> Vaaaaai </p>
						<p> Compra bebida de litro </p>
						<p> E chapa com ela </p>
						<p> Faz essa noite </p>
						<p> A melhor da vida dela </p>
						
					</section> 
					
					
					
					<aside id="noticia2">
						<h2> Banda Cover - Zé Neto e Cristiano </h2>
						
						<p> Vem ser a minha semana </p>
						<p> Meu fim de semana e o meu feríado </p>
						<p> Meu remédio controlado </p>
						<p> O meu disco arranhado </p>
						<p> Naquela parte que diz eu te amo, te amo </p>
						<p> Te amo, te amo, te amo, te amo, te amo, te amo. </p>
					</aside>
					
					
				</div>
				
				
				<h1 id="fraseMotivacional"> O amor é a sabedoria dos loucos e a loucura dos sábios. - Samuel Johnson </h1>
				
			
			</div>
		 
			
		</div>
		
		
<!--=======================================================================================================================================-->
		
		
		<div id="rodape">
			
			<p id="campos">
				<a href="termosDeServicos.php" id="i"> Termos de Serviço </a> //
					
				<a href="referenciasBibliográfica.php" id="i"> Referencias Bibliográfica </a>
			</p>
				
			<footer>
				
				<p> Copyright &copy; 2020 - by João Ferreira <br/>
				<a href="https://www.facebook.com/profile.php?id=100013593917417" target="_blank" id="facebook"> Facebook</a> |
				
				<a href="https://www.instagram.com/joao.ferreira9899" target="_blank" id="instagram"> Instagram </a>
				</p>
				
			</footer>
			
		</div>
		
		
	</body>


</html>




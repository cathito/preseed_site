<?php

session_start();
if(!isset($_SESSION['id_usuario'])){
	$id_usuario=$_SESSION['id_usuario'];
	header("location: login.php");
	exit;
}


$areaConhecimento = addslashes($_POST['areaConhecimento']);
$alternativaCorreta = addslashes($_POST['alternativaCorreta']);
$nivelDificuldade = addslashes($_POST['nivelDificuldade']);

$dataDaPublicacao = addslashes($_POST['data']);

$pergunta = addslashes($_POST['pergunta']);
$respostaA = addslashes($_POST['respostaA']);
$respostaB = addslashes($_POST['respostaB']);
$respostaC = addslashes($_POST['respostaC']);
$respostaD = addslashes($_POST['respostaD']);
$respostaE = addslashes($_POST['respostaE']);

$gravarQuestao = addslashes($_POST['gravarQuestao']);

/*==============================================================================*/

$servername = "localhost";
$database = "preseed";
$username = "root";
$password = "";

$conn = mysqli_connect($servername, $username, $password, $database);
	
if (!$conn) {
	echo "<script language='javascript' type='text/javascript'>
			alert('Falha Na conexao Com o Banco de Dados!\nPedimos Desculpas Pelo Transtorno!');
			window.location.href='../cadastrar.php';
		</script>";
	die();
}

/*==============================================================================*/

if(isset($gravarQuestao)) {
	/*echo "<script language='javascript' type='text/javascript'>
				alert('Passou pelo evento click');
				window.location.href='../lancarSimulado.php';
			</script>";*/
	
	if( ($areaConhecimento == "" || $areaConhecimento == null) ||
		($alternativaCorreta == "" || $alternativaCorreta == null) || 
		($nivelDificuldade == "" || $nivelDificuldade == null) ){
		echo "<script language='javascript' type='text/javascript'>
				alert('Todos os Informativos Deverão Ser Preenchidos!');
				window.location.href='../lancarSimulado.php';
			</script>";
			
			
	}elseif( ($pergunta == "" || $pergunta == null) || 
			($respostaA == "" || $respostaA == null) ||
			($respostaB == "" || $respostaB == null) ||
			($respostaC == "" || $respostaC == null) ||
			($respostaD == "" || $respostaD == null) ||
			($respostaE == "" || $respostaE == null) ){
		
		echo "<script language='javascript' type='text/javascript'>
				alert('Todos as Informações  Pertecentes a Questão Deverão Ser Preenchidas!');
				window.location.href='../lancarSimulado.php';
			</script>";
			
	}else{
		$sql = "insert into questoeslancadas values (default, '".$_SESSION['id_usuario']."', '".$areaConhecimento."', '".$alternativaCorreta."', '".$nivelDificuldade."', '".$dataDaPublicacao."', '".$pergunta."', '".$respostaA."', '".$respostaB."', '".$respostaC."', '".$respostaD."', '".$respostaE."');";
		
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			echo "<script language='javascript' type='text/javascript'>
						alert('Questão Cadastrada com Sucesso!');
						window.location.href='../lancarSimulado.php';
					</script>";
			
		}else {
			echo "<script language='javascript' type='text/javascript'>
						alert('Falha de Execução!\nPedimos Desculpas Pelo Transtorno!');
						window.location.href='../lancarSimulado.php';
					</script>";
		}
		
	}
	
	
}
/*==============================================================================*/

?>
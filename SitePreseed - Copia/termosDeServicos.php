<!DOCTYPE html>

<html lang = "pt-br">

	<head> 
		<meta charset = "UTF-8"/>
		<title> Simula Pre-seed</title>
		<link rel = "stylesheet" href = "_css/t_inicio.css"/>
		<link rel = "stylesheet" href = "_css/termoServico.css"/>
	</head>
<!--==================================================================-->

	<body>
	
		<div id="cabecalho"> 
			<header id = "cabecalho">
				<nav id = "menu">
					<ul type = "default" ><!-- start = "default"> para ol-->
						<li> <a href ="	index.php"> Página Inicial </a> </li>
						
						<li> <a href ="login.php"> Login </a> </li>
						
						<li> <a href ="escolhaSimulado.php"> Simulados </a> </li>
						
						<li> <a href ="faleConosco.php"> Fale conosco </a> </li>
					</ul>
				</nav>
				
				<img id="icone" src = "_imagens/logocp.png"/>
				
			</header>
		</div>
		
		<div id="corp">
			<h1> Termos de Serviço </h1>
			
			<p> </p>
			
			<fieldset> <legend> &nbsp; CONDUTA DO USUARIO &nbsp; </legend>
			 
				<p>
					<p> Você entende que todas as informações, dados, texto, fotografias, gráficos, mensagens, tags ou outros materiais, publicados publicamente ou transmitidos em particular, são de responsabilidade exclusiva da pessoa, de quem esse Conteúdo se originou. Isso significa que você, e não o ------------, é totalmente responsável por todo o conteúdo que você enviar, publicar, enviar por e-mail, transmitir ou disponibilizar por meio do Serviço. </p>
					
					<p> O ------- não controla o Conteúdo postado através do Serviço e, como tal, não garante a precisão, a integridade ou a qualidade desse Conteúdo. Você entende que, ao usar o Serviço, pode ser exposto a Conteúdo ofensivo, pornográfico, indecente ou censurável. Sob nenhuma circunstância a ---------- será responsável de qualquer forma por qualquer Conteúdo, incluindo, entre outros.</p>

					<p> Você concorda em não usar o Serviço para: </p>
					
					<p> (01) - publicar, enviar por e-mail, transmitir ou disponibilizar qualquer Conteúdo que seja ilegal, prejudicial, ameaçador, abusivo, hostil, tortuoso, difamatório, vulgar, obsceno, difamatório, invasivo da privacidade de outra pessoa, odioso ou racial, étnico ou de outra forma censurável; </p>
					<p> (02) - publicar qualquer conteudo de natureza é/ou cunho pornográfica. </p>
					<p> (03) - prejudicar pessoas fisicas é/ou juridicas de qualquer forma; </p>
					
				</p>
			</fieldset>
			
			
			<fieldset> <legend> &nbsp; CONTA, SENHA E SEGURANÇA DO USUARIO  &nbsp; </legend>
				<p>
					<p> Ao se cadastar em nossos servisos o usuario fornecera uma designação de conta(Login) e uma senha, sendo o responsável por manter a confidencialidade da senha e da conta e é totalmente responsável por todas as atividades que ocorram sob sua senha ou conta.</p>

					<p> O usuario deve certifiquecar-se de sair da sua conta no final de cada sessão. O ______ não pode e não será responsável por qualquer perda ou dano decorrente do seu não cumprimento desta Seção.</p>
				</p>
			</fieldset>
			
			
			<fieldset> <legend> &nbsp; SEM REVENDA DE SERVIÇO &nbsp; </legend>
			
				<p> Você concorda em não vender, negociar, revender ou explorar para quaisquer fins comerciais, qualquer parte do Serviço, uso do Serviço ou acesso ao Serviço.</p>
				
			</fieldset>
			
			
			<fieldset> <legend> &nbsp; ESTABILIDADE DO SERVIÇO FORNECIDO &nbsp; </legend>
			
				<p> 
					<p> Os nossos servicos estão limitados aos recursos oferecidos e mantidos pelo InfinityFree, consequentemente quaisquer acao é/ou interferencia nos servicos e recursos do InfinityFree ira nos afetar diretamente.</p>
					
					<p> Em caso de eventuais problemas relacionados diretamente ao codigo da aplicação ------ estaremos dando a manutenção necessaria. </p>
				</p>
				
			</fieldset>
			
			
			<fieldset> <legend> &nbsp; CUSTO PELO USO DO SERVICO &nbsp; </legend>
			
				<p> O nosso servico é totalmente gratuito. </p>
				
			</fieldset>
			
		</div>
		
<!--==================================================================-->
		
		<div id="rodape">
			
			<p id="campos">
				<a href="termosDeServicos.php" target="_blank" id="i"> Termos de Serviço </a> //
					
				<a href="referenciasBibliográfica.php" target="_blank" id="i"> Referencias Bibliográfica </a>
			</p>
				
			<footer>
				
				<p> Copyright &copy; 2020 - by João Ferreira <br/>
				<a href="https://www.facebook.com/profile.php?id=100013593917417" target="_blank" id="facebook"> Facebook</a> |
				
				<a href="https://www.instagram.com/joao.ferreira9899" target="_blank" id="instagram"> Instagram </a>
				</p>
				
			</footer>
			
		</div>
		
		
	</body>


</html>
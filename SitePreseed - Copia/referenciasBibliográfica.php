<!DOCTYPE html>

<html lang = "pt-br">

	<head> 
		<meta charset = "UTF-8"/>
		<title> Simula Pre-seed</title>
		<link rel = "stylesheet" href = "_css/t_inicio.css"/>
		<link rel = "stylesheet" href = "_css/referencias.css"/>
	</head>
<!--==================================================================-->

	<body>
	
		<div id="cabecalho"> 
			<header id = "cabecalho">
				<nav id = "menu">
					<ul type = "default" ><!-- start = "default"> para ol-->
						<li> <a href ="	index.php"> Página Inicial </a> </li>
						
						<li> <a href ="login.php"> Login </a> </li>
						
						<li> <a href ="escolhaSimulado.php"> Simulados </a> </li>
						
						<li> <a href ="faleConosco.php"> Fale conosco </a> </li>
					</ul>
				</nav>
				
				<img id="icone" src = "_imagens/logocp.png"/>
				
			</header>
		</div>
		
		
		<div>
			
			<h1> Referencias </h1>
			
			<fieldset>
				<p>
					Imagem de fundo da Tela inicial:<br/>
					<a href="https://www.pexels.com/photo/people-notes-meeting-team-7095/" target="_blank"> Foto de fotografias de inicialização de Pexels </a>
				</p>
			</fieldset>
			
			<br/>
			<fieldset>
				<p>
					Imagem de fundo da: Tela de Login, Tela de Cadastro:<br/> Foto de Kaique Rocha no Pexels <br/>
					<a href="https://www.pexels.com/pt-br/foto/ao-ar-livre-arvores-cenico-clima-1591447/" target="_blank"> Foto de Guillaume Meurice no Pexels </a>
				</p>
			</fieldset>
			
			<br/>
			<fieldset>
				<p>
					Imagem de fundo da Tela de: Lancar Questão, Questoes Lancadas, Resolução Simulado<br/>
					<a href="https://www.pexels.com/pt-br/foto/abajur-alvenaria-argamassa-argila-276514/" target="_blank"> Foto de Pixabay no Pexels </a>
				</p>
			</fieldset>
			
		</div>
		
		<p> </p><br/><br/>
<!--==================================================================-->
		
		<div id="rodape">
			
			<p id="campos">
				<a href="termosDeServicos.php" target="_blank" id="i"> Termos de Serviço </a> //
					
				<a href="referenciasBibliográfica.php" target="_blank" id="i"> Referencias Bibliográfica </a>
			</p>
				
			<footer>
				
				<p> Copyright &copy; 2020 - by João Ferreira <br/>
				<a href="https://www.facebook.com/profile.php?id=100013593917417" target="_blank" id="facebook"> Facebook</a> |
				
				<a href="https://www.instagram.com/joao.ferreira9899" target="_blank" id="instagram"> Instagram </a>
				</p>
				
			</footer>
			
		</div>
		
	</body>


</html>
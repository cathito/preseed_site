<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Menu do Usuario </title>
		<link rel="stylesheet" href="_css/gerarSimulado.css">
		
		<script>
		
			function pegarData(){
				var data = new Date();
				
				var dia = data.getDate();
				var mes     = data.getMonth();          // 0-11 (zero=janeiro)
				var ano    = data.getFullYear();       // 4 dígitos
				mes++;
				if(mes<10){
					mes="0"+mes;
				}
				
				var datinha =ano+"-"+mes+"-"+dia;
				
				document.getElementById("data").value= datinha;
			}
		</script>
		
	</header>
	
	
	<body>
	
		<form method="POST" action="classes/gravarSimulado.php">
			
			<h1> Seja Bem Vindo(a)! </h1>
			
			<p id="dataInv"> <input type="text" name="data" id="data" placeholder="data..." value=""/> </p>
			
			
			
			<fieldset id="infoUsuario"> <legend> &nbsp; Caderno (01) &nbsp;</legend>
				
				<?php
					$conn = mysqli_connect("localhost", "root", "", "preseed"); // ---------- Iniciar Conexao
					
					$ouveErro= false;
					$msgErro="<script language='javascript' type='text/javascript'>
								alert('Falha Na conexao Com o Bacco de Dados!\nPedimos Desculpas Pelo Transtorno!');
								window.location.href='../login.php';
							</script>";
				
					if ($conn){
						$sql = "select count(*) from questoeslancadas where areaDoConheciento = 'Linguagens, Códigos e suas Tecnologias' and id_simulado=0;";
						$resultado = mysqli_query($conn, $sql);
						
						if ($resultado) {
							if( ($dadosLinguagem = mysqli_fetch_array($resultado)) == null){
								$ouveErro=true;
							}
							
						}else {
							$ouveErro=true;
						}
						
			/*====================================================================================================================================*/
			
						$sql = "select count(*) from questoeslancadas where areaDoConheciento = 'Ciências Humanas e suas Tecnologias' and id_simulado=0;";
						$resultado = mysqli_query($conn, $sql);
						
						if ($resultado) {
							if( ($dadosHumanas = mysqli_fetch_array($resultado)) == null){
								$ouveErro=true;
							}
							
						}else {
							$ouveErro=true;
						}
						
					}else{
						$ouveErro=true;
					}
					
					
					
					
					
					if($ouveErro){
						echo $msgErro;
					}else{
						echo "<p> 
							Provas de Linguagens, Códigos e Suas Tecnologias. --- Questões Lancadas: ".$dadosLinguagem[0]."<br/>
							Ciências Humanas e Suas Tecnologias. --- Questões Lancadas: ".$dadosHumanas[0]."; 
						</p>";
					}
				?>
				<input type="submit" name="caderno" value="Gerar Caderno(01)" onmousemove="pegarData()"/>
				
			</fieldset>
			
			
			<fieldset id="infoUsuario"> <legend> &nbsp; Caderno (02) &nbsp;</legend>
				
				<?php
					$conn = mysqli_connect("localhost", "root", "", "preseed"); // ---------- Iniciar Conexao
					
					$ouveErro= false;
					$msgErro="<script language='javascript' type='text/javascript'>
								alert('Falha Na conexao Com o Bacco de Dados!\nPedimos Desculpas Pelo Transtorno!');
								window.location.href='../login.php';
							</script>";
				
					if ($conn){
						$sql = "select count(*) from questoeslancadas where areaDoConheciento = 'Ciências da Natureza e suas Tecnologias' and id_simulado=0;";
						$resultado = mysqli_query($conn, $sql);
						
						if ($resultado) {
							if( ($dadosCiencias = mysqli_fetch_array($resultado)) == null){
								$ouveErro=true;
							}
							
						}else {
							$ouveErro=true;
						}
						
			/*====================================================================================================================================*/
			
						$sql = "select count(*) from questoeslancadas where areaDoConheciento = 'Matemática e suas Tecnologias' and id_simulado=0;";
						$resultado = mysqli_query($conn, $sql);
						
						if ($resultado) {
							if( ($dadosMatematica = mysqli_fetch_array($resultado)) == null){
								$ouveErro=true;
							}
							
						}else {
							$ouveErro=true;
						}
						
					}else{
						$ouveErro=true;
					}
					
			/*====================================================================================================================================*/
					
					if($ouveErro){
						echo $msgErro;
					}else{
						echo "<p> 
							Provas de Ciências da Natureza e Suas Tecnologias. --- Questões Lancadas: ".$dadosCiencias[0]."<br/>
							Matemática e Suas Tecnologias. --- Questões Lancadas: ".$dadosMatematica[0]."; 
						</p>";
					}
				?>
				
				
				<input type="submit" name="caderno" value="Gerar Caderno(02)" onmousemove="pegarData()"/> 
				
				
				
			</fieldset>
			
			
		</form>
		
	</body>
	
	
</html>
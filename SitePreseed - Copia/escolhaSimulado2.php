<!DOCTYPE html>

<html lang = "pt-br">

	<head> 
		<meta charset = "UTF-8"/>
		<title> Simula Pre-seed</title>
		<link rel = "stylesheet" href = "_css/t_inicio.css"/>
		<link rel = "stylesheet" href = "_css/simulados.css"/>
	</head>
<!--==================================================================-->

	<body>
		<div id="interface"> 
			<header id = "cabecalho">
			
				<nav id = "menu">
					<h1>Menu Principal </h1>
					
					<ul type = "default" ><!-- start = "default"> para ol-->
						<li> <a href ="	index.php"> Página Inicial </a> </li>
						
						<li> <a href ="login.php"> Login </a> </li>
						
						<li> <a href ="escolhaSimulado.php"> Simulados </a> </li>
						
						<li> <a href ="faleConosco.php"> Fale conosco </a> </li>
						
					</ul>
				</nav>
				
			</header>
		<!--=========================================================-->
			<section id="corpo-full">
			
			
				<p id="apresentacao"> Tenha Acesso a Todos os Nossos Simulados! </p>
				
				
				
				
				<article id="simuladosAbertos">
				
					<fieldset id="simAberto"> <legend> Simulados Em Aberto &nbsp; </legend> 
					
						<?php
						$conn = mysqli_connect("localhost", "root", "", "preseed"); // ---------- Iniciar Conexao
						
							$sql = "select * from simuladosLancados where emAberto=1;";
							$buscaSimulado = mysqli_query($conn, $sql);
							
							if($buscaSimulado){
								
								While($dados = mysqli_fetch_assoc($buscaSimulado)){
									echo "<p> Simulado (".$dados["id"].") - ".$dados["dataDaPublicacao"]." &nbsp;&nbsp;&nbsp;&nbsp;
											<br/>".$dados["descrissao"]."
											
											&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
											<a href='' target='_blank'> Comecar </a> 
											
											<br/> <br/>
										</p>";
									
								}
							}else{
								echo "<script language='javascript' type='text/javascript'>
									alert('Falha Na conexao Com o Bacco de Dados!\nPedimos Desculpas Pelo Transtorno!');
									window.location.href='../areaPrivada.php';
								</script>";
							}
							
						?>
					
					</fieldset>
					
				</article>
				
				<p> <br/> </p>
				
				<article id="simuladosAnteriores">
					<fieldset id="simAnterior"> <legend> Simulados Anteriores &nbsp; </legend> 
						<?php
						$conn = mysqli_connect("localhost", "root", "", "preseed"); // ---------- Iniciar Conexao
						
							$sql = "select * from simuladosLancados where emAberto=0;";
							$buscaSimulado = mysqli_query($conn, $sql);
							
							if($buscaSimulado){
								
								While($dados = mysqli_fetch_assoc($buscaSimulado)){
									echo "<p> Simulado (".$dados["id"].") - ".$dados["dataDaPublicacao"]." &nbsp;&nbsp;&nbsp;&nbsp;
											<br/>".$dados["descrissao"]."
											
											&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
											<a href='' target='_blank'> Visitar </a> 
											
											<br/> <br/>
										</p>";
									
								}
							}else{
								echo "<script language='javascript' type='text/javascript'>
									alert('Falha Na conexao Com o Bacco de Dados!\nPedimos Desculpas Pelo Transtorno!');
									window.location.href='../areaPrivada.php';
								</script>";
							}
							
						?>
					</fieldset>
					
				</article>
				
				<p> </p>
			</section>
			
			
			
			
		<!--========================================================-->
		
			<div id="rodape">
			<footer id="rodape">
				<p> Copyright &copy; 2020 - by João Ferreira <br/>
				<a href="https://www.facebook.com/profile.php?id=100013593917417" target="_blank" id="facebook"> Facebook</a> |
				
				<a href="https://www.instagram.com/joao.ferreira9899" target="_blank" id="instagram"> Instagram </a>
				</p>
				
			</footer>
			</div>
			
		<!--========================================================-->
		
		</div>
	<body>


</html>
<?php
	
	session_start();
	if(!isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}
	
?>

<!DOCTYPE html>

<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Projeto Login </title>
		<link rel="stylesheet" href="_css/editarPerfil.css">
		
		<script>
			
			var edPerfil=false;
			var edLogin=false;
			
			function edicaoPerfil() {
				
				if(edPerfil){
					document.getElementById('edicaoDoUsuario').value="--";
					document.getElementById('edPerf').innerHTML = "Salvar Edição";
					document.getElementById('edPerf').style.backgroundColor = "#ff2f00";
					edPerfil= false;
				}else{
					document.getElementById('edicaoDoUsuario').value="ok";
					document.getElementById('edPerf').innerHTML = "Cancelar";
					document.getElementById('edPerf').style.backgroundColor = "#606060";
					edPerfil=true;
				}
			}
			
			
			function edicaoLogin() {
				
				if(edLogin){
					document.getElementById('edicaoDoLogin').value="--";
					document.getElementById('edLog').innerHTML = "Salvar Edição";
					document.getElementById('edLog').style.backgroundColor = "#ff2f00";
					edLogin= false;
				}else{
					document.getElementById('edicaoDoLogin').value="ok";
					document.getElementById('edLog').innerHTML = "Cancelar";
					document.getElementById('edLog').style.backgroundColor = "#606060";
					edLogin=true;
				}
				
			}
			
		</script>
		
		
	</header>
	
	
	<body>
		
		<div id="corpo">
			
			<fieldset id="infoUsuario"> <legend> &nbsp; Perfil Atual &nbsp;</legend>
				
				<?php
					$conn = mysqli_connect("localhost", "root", "", "preseed"); // ---------- Iniciar Conexao
					if ($conn){
						$sql = "select * from usuario where id_login = '".$_SESSION['id_usuario']."';";
						$resultado = mysqli_query($conn, $sql);
						
						if ($resultado) {
							if( ($dados = mysqli_fetch_array($resultado)) != null){
								echo " <p> $dados[2] </p> 
									   <p> $dados[3] </p> 
									   <p> $dados[4] </p>";
							}else{
								echo "<script language='javascript' type='text/javascript'>
										alert('Falha Na conexao Com o Bacco de Dados!\nPedimos Desculpas Pelo Transtorno!');
										window.location.href='../login.php';
									</script>";
								/*echo "Erro ao capturar dados do cliente";*/
							}
							
						}else {
							echo "<script language='javascript' type='text/javascript'>
								alert('Falha Na conexao Com o Bacco de Dados!\nPedimos Desculpas Pelo Transtorno!');
								window.location.href='../login.php';
							</script>";
							/*echo "Error: " . $sql . "<br>" . mysqli_error($conn);*/
						}
						
					}else{
						echo "<script language='javascript' type='text/javascript'>
								alert('Falha Na conexao Com o Bacco de Dados!\nPedimos Desculpas Pelo Transtorno!');
								window.location.href='../login.php';
							</script>";
						/*die("Falha Na Conexao: " . mysqli_connect_error());*/
					}
				?>
			</fieldset>
			
<!--=====================================================================================================-->
<!--=====================================================================================================-->
			
			<div id="edicao">
			
				<h1> Editar Perfil/Login </h1>
				
				<section id="usuario">
					<h2> Editar Perfil </h2>
					<input type="text" placeholder="Nome Completo" name="nome" maxlength="40" />
					<input type="email" placeholder="Email" name="email" maxlength="40"/>
					<input type="text" placeholder="Polo" name="polo" maxlength="20"/>
					
					<p> <button id="edPerf" onclick="edicaoPerfil()"> Salvar Edição </button> </p>
					
					
				</section>
				
				<section id="login">
					<h2> Editar Login </h2>
					<input type="text" placeholder="Novo Login" name="login" maxlength="12"/>
					<input type="password" placeholder="Nova Senha" name="senha" maxlength="12"/>
					
					<p> <button id="edLog" onclick="edicaoLogin()"> Salvar Edição </button> </p>
					
				</section>
				
			</div>
			
			<input type="text" value="" id="edicaoDoUsuario" name="edicaoDoUsuario"/>
			<input type="text" value="" id="edicaoDoLogin" name="edicaoDoLogin"/>
			
			<fieldset id="logar"> <legend> &nbsp; Logar &nbsp; </legend>
				<form method="POST" action="classes/edPerfil.php">
					<input type="text" placeholder="Login" name="login" maxlength="12"/>
					<input type="password" placeholder="Senha" name="senha" maxlength="12"/>
					<input type="submit" value="Finalizar Edição" id="finalizar" name="finalizar">
				</form>
			</fieldset>
			
			
		</div>
		
	</body>
	
	
</html>
<?php
	
	include_once("conexao.php");

	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}else{
		global $conn;
		$sql = "select nivelDeAceso from logonn where id_login = ".$_SESSION['id_login'].";";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			$dados = mysqli_fetch_array($resultado);
			
			if($dados[0]!="Coordenador"){
				echo"<script language='javascript' type='text/javascript'> 
						alert('Acesso Negado!');
						window.history.back();
					</script>";
			}
		}else {
			echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (01)');
				window.history.back();
			</script>";
		}
	}
/*======================================================================================================*/


	$dataDaPublicacao = htmlspecialchars(addslashes($_POST['data']));
	$cad1 = htmlspecialchars(addslashes($_POST['caderno']));
	$n_questoesCad1 = htmlspecialchars(addslashes($_POST['n_questoes1']));
	$n_questoesCad2 = htmlspecialchars(addslashes($_POST['n_questoes2']));

	if($cad1=="Gerar Caderno(01)"){
		$descrissao = "Linguagens // Humanas";
		$areaConhecimento1 = "Linguagens, Códigos e suas Tecnologias";
		$areaConhecimento2 = "Ciências Humanas e suas Tecnologias";
		$n_questoes = $n_questoesCad1;
	}else{
		$descrissao ="Narturezas // Matemática";
		$areaConhecimento1 = "Ciências da Natureza e suas Tecnologias";
		$areaConhecimento2 = "Matemática e suas Tecnologias";
		$n_questoes = $n_questoesCad2;
	}

	$IdSimulado;

/*======================================================================================================*/

	if(isset($cad1)) {
		if( $n_questoes >= 3){
			if(gerarSimulado($dataDaPublicacao, $descrissao)){
				if(buscarIdSimulado($descrissao)){
					indicarQuestaoSimulado($areaConhecimento1, $areaConhecimento2);
				}
			}
		}else{
			echo "<script >
				alert('Não é Possivel Gerar Simulado com Menos de 3 Questoes!');
				window.history.back();
			</script>";
		}
	}



/*======================================================================================================*/

function gerarSimulado($dataDaPublicacao, $descrissao){
	global $conn;
	
	$sql = "insert into simuladosLancados values (default, ".$_SESSION['id_usuario'].", '".$dataDaPublicacao."', '".$descrissao."', default);";
	$resultado = mysqli_query($conn, $sql);
	
	if($resultado) {
		return true;
	}else{
		echo "<script >
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (01)');
				window.history.back();
			</script>";
		return false;
	}
}


function buscarIdSimulado($descrissao){
	global $conn;
	global $IdSimulado;
	
	$sql = "select id_simuladoLancado from simuladosLancados where descrissao= '".$descrissao."' and id_usuario= ".$_SESSION['id_usuario'].";";
	$resultado = mysqli_query($conn, $sql);
	
	if($resultado){
		$IdSimulado = mysqli_fetch_array($resultado);
		return true;
	}else{
		echo "<script >
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (02)');
				window.history.back();
			</script>";
		return false;
	}
}


function indicarQuestaoSimulado($areaConhecimento1, $areaConhecimento2){
	global $conn;
	global $IdSimulado;
	
	$sql = "UPDATE questoesLancadas SET id_simulado = ".$IdSimulado[0]." where id_simulado = 0 and (areaDoConheciento= '".$areaConhecimento1."' or areaDoConheciento= '".$areaConhecimento2."') ;";
	$resultado = mysqli_query($conn, $sql);
	
	if($resultado){
		echo "<script language='javascript' type='text/javascript'>
			alert('Simulado Gerado Com Sucesso!');
			window.location.href='../gerarSimulado.php';
		</script>";
	}else{
		echo "<script >
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (03)');
				window.history.back();
			</script>";
		return false;
	}
}


/*======================================================================================================*/

?>
<?php
	include_once("conexao.php");
	
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}else{
		global $conn;
		$sql = "select nivelDeAceso from logonn where id_login = ".$_SESSION['id_login'].";";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			$dados = mysqli_fetch_array($resultado);
			
			if( $dados[0]!="Professor" && $dados[0]!="Coordenador"){
				echo"<script language='javascript' type='text/javascript'> 
						alert('Acesso Negado!');
						window.history.back();
					</script>";
			}
		}else {
			echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (01)');
				window.history.back();
			</script>";
		}
	}
/*===================================================================================================*/


	$areaConhecimento = htmlspecialchars(addslashes($_POST['areaConhecimento']));
	$alternativaCorreta = htmlspecialchars(addslashes($_POST['alternativaCorreta']));
	$nivelDificuldade = htmlspecialchars(addslashes($_POST['nivelDificuldade']));

	$pergunta = htmlspecialchars(addslashes($_POST['pergunta']));
	$respostaA = htmlspecialchars(addslashes($_POST['respostaA']));
	$respostaB = htmlspecialchars(addslashes($_POST['respostaB']));
	$respostaC = htmlspecialchars(addslashes($_POST['respostaC']));
	$respostaD = htmlspecialchars(addslashes($_POST['respostaD']));
	$respostaE = htmlspecialchars(addslashes($_POST['respostaE']));

	$gravarQuestao = htmlspecialchars(addslashes($_POST['gravarQuestao'])); 
	
	$id_questao = htmlspecialchars(addslashes($_POST['id_questao']));
	
	
/*===================================================================================================*/


	if(isset($gravarQuestao)) {
		
		if( ($areaConhecimento == "" || $areaConhecimento == null) ||
			($alternativaCorreta == "" || $alternativaCorreta == null) || 
			($nivelDificuldade == "" || $nivelDificuldade == null) ){
			echo "<script language='javascript' type='text/javascript'>
					alert('Todos os Informativos Deverão Ser Preenchidos!');
					window.history.back();
			</script>";
		
		
		}elseif( ($pergunta == "" || $pergunta == null) || ($respostaA == "" || $respostaA == null) ||
				($respostaB == "" || $respostaB == null) || ($respostaC == "" || $respostaC == null) ||
				($respostaD == "" || $respostaD == null) || ($respostaE == "" || $respostaE == null) ){
			
			echo "<script language='javascript' type='text/javascript'>
					alert('Todos as Informações  Pertecentes a Questão Deverão Ser Preenchidas!');
					window.history.back();
			</script>";
			
		}else{
			
			$sql = "UPDATE questoesLancadas SET areaDoConheciento = '".$areaConhecimento."', alternativaCorreta = '".$alternativaCorreta."', 
			nivelDificuldade = '".$nivelDificuldade."',  pergunta = '".$pergunta."', respostaA = '".$respostaA."', respostaB = '".$respostaB."', 
			respostaC = '".$respostaC."', respostaD = '".$respostaD."', respostaE = '".$respostaE."' WHERE id_questao=".$id_questao.";";
			
			$resultado = mysqli_query($conn, $sql);
			
			if ($resultado) {
				echo"<script language='javascript' type='text/javascript'> 
						alert('Questão Editada com Sucesso!');
						window.location.href='../questoesLancadas.php';
					</script>";
			}else {
				echo "<script language='javascript' type='text/javascript'>
						alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 02');
						window.history.back();
					</script>";
			}
			
		}
	}



?>
<?php
	
	include_once("conexao.php");

	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}else{
		global $conn;
		$sql = "select nivelDeAceso from logonn where id_login = ".$_SESSION['id_login'].";";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			$dados = mysqli_fetch_array($resultado);
			
			if($dados[0]!="Coordenador"){
				echo"<script language='javascript' type='text/javascript'> 
						alert('Acesso Negado!');
						window.history.back();
					</script>";
			}
		}else {
			echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (01)');
				window.history.back();
			</script>";
		}
	}
/*============================================================================================================*/

	$email = htmlspecialchars(addslashes($_POST['email']));
	$cadastrar = htmlspecialchars(addslashes($_POST['cadastrar']));
	
	$id_Professor;
 /*---------------------------------------------------------------------*/
	
	if(isset($cadastrar)) {
		if(camposPreenchidos($email)){
			if(buscarLogon($email)){
				gerarProfessor();
			}
		}
		
	}
	
/*============================================================================================================*/
	
	function camposPreenchidos($email){ 
	
		if($email == "" || $email == null) {
			echo "<script >
					alert('Todos os Campos Devem Ser Preenchidos!');
					window.history.back();
				</script>";
			return false;
		}
		return true;
	}
	
/*============================================================================================================*/

	function buscarLogon($email){
		global $conn;
		global $id_Professor;
		
		$sql = "select id_login from usuario where email = '".$email."';";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			if(($id_Professor = mysqli_fetch_array($resultado)) != null){
				return true;	
			}else{
				echo "<script >
					alert('Email Não Encontrado Em Nossos Cadastros!');
					window.history.back();
				</script>";
				
				return false;
			}
		}else{
			echo "<script >
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (02)');
					window.history.back();
				</script>";
		}
		
		return false;
	
	}
/*============================================================================================================*/
	
	function gerarProfessor(){
		global $conn;
		global $id_Professor;
		
		$sql = "UPDATE logonn SET nivelDeAceso = 'Professor' WHERE id_login = ".$id_Professor[0].";";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			echo "<script language='javascript' type='text/javascript'>
					alert('Professor Cadastrado com Sucesso!');
					window.close();
				</script>";
		}else {
			echo "<script >
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (03)');
				window.history.back();
			</script>";
		}
	}
	
/*============================================================================================================*/
	
	
	
?>
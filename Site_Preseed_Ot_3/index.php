
<!DOCTYPE html>

<html lang = "pt-br">

	<head> 
		<meta charset = "UTF-8"/>
		<title> Simulando </title>
		<link rel = "stylesheet" href = "_css/t_inicio.css"/>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel="sortcut icon" href="_imagens/logo2.png" type="image/png" />
	</head>
<!--==================================================================-->

	<body>
		
		
		<header id = "cabecalho">
			<nav id = "menu">
				<ul type = "default" ><!-- start = "default"> para ol-->
					<li> <a href ="	index.php"> Página Inicial </a> </li>
					
					<li> <a href ="login.php"> Login </a> </li>
					
					<li> <a href ="escolhaSimulado.php"> Simulados </a> </li>
					
					<li> <a href ="faleConosco.php"> Fale conosco </a> </li>
					<li> // < PROJETO BETA > \\ </li>
				</ul>
			</nav>
			
			<img id="icone" src = "_imagens/logo2.png"/>
			
			
		</header>
		
	
<!--=======================================================================================================================================-->
	
		<div> <!--=== principal ===-->
			
			<img id="inicio" src = "_imagens/people-notes-meeting-team-7095.jpg"/>
			
			<h1 id="recepcao"> SEJAM BEM VINDO(AS)! </h1>
			
			
			<div id="desenvolvimento"> 
			
				<div id="corpo">
					<h1> Exame Nacional do Ensino Médio (ENEM) </h1>
					
					<p> O Exame Nacional do Ensino Médio (Enem) é uma prova realizada anualmente sendo aplicado em dois dias de prova, contendo 180 questões objetivas (divididas em quatro grandes áreas) e uma questão de redação.</p> 
					
					<p> O Exame Nacional do Ensino Médio (Enem) é utilizado para avaliar a qualidade do ensino médio no país. Seu resultado serve para acesso ao ensino superior brasileiro em universidades públicas e particulares. </p>
					
					<p> O acesso ao ensino superior em universidades públicas ocorre através do Sistema de Seleção Unificada (SiSU), em universidades particulares através do Programa Universidade para Todos (ProUni) ou através do Fundo de Financiamento ao Estudante do Ensino Superior (Fies). </p>
					
					<br/>
					<h2> Novidades </h2> 
					
					<p> O Instituto Nacional de Estudos e Pesquisas Educacionais Anísio Teixeira (Inep) anunciou como uma das novidades no Exame Nacional do Ensino Médio (Enem) para 2020 a aplicação de provas digitais, juntamente à versão tradicional (impressa).</p>
					
					<p> A princípio, as provas digitais não irá atender a todos os participantes. De acordo com o edital, o Inep vai aplicar o Enem Digital para 101.100 participantes. A intenção do Ministério da Educação (MEC) é aumentar a cada ano a quantidade de participantes que farão as provas digitais, até elas substituírem totalmente as provas impressas, em 2026, visando reduzir custos de impressão.</p>
					
					<p> Vale ressaltar que não é possível participar da prova digital e impressa em uma mesma versão do Enem. O estudante deve optar por apenas uma das provas. </p>
					
				</div>
				
				
				<h1 id="ultimasNoticias"> Últimas Notícias! </h1>
				
				
				<div id="noticiario">
					<section id="noticia1"> 
						<h1> Sobre o ENEM. </h1>
						
						<h2> Enem foi adiado! </h2>
						
						<p> Em função do impacto da pandemia do coronavírus, Instituto Nacional de Estudos e Pesquisas Educacionais Anísio Teixeira (Inep) e o  Ministério da Educação (MEC) decidiram pelo adiamento da aplicação do exame nas versões impressa e digital. </p>
						
						<p> Foi estabelecido que o exame na sua versão digital será realizado nos dias 17 e 24/1/2021, na versão digital nos dias - 31/1 e 7/2/2021. </p>
						
						<p> saiba mais em: <a href="https://enem.inep.gov.br/"target="_blank"> https://enem.inep.gov.br/ </a>.
						
					</section> 
					
					<br/>
					
					<aside id="noticia2">
						
						<h1> Sobre Nos. </h1>
						
						<p> Seguimos ouvindo os grandes hits do momento, hoje vamos de: </p>
						
						<h2> Banda Cover - Zé Neto e Cristiano </h2>
						
						<p> Vem ser a minha semana </p>
						<p> Meu fim de semana e o meu feríado </p>
						<p> Meu remédio controlado </p>
						<p> O meu disco arranhado </p>
						<p> Naquela parte que diz eu te amo, te amo </p>
						<p> Te amo, te amo, te amo, te amo, te amo, te amo. </p>
					</aside>
					
					
				</div>
				
				
				<h1 id="fraseMotivacional"> O amor é a sabedoria dos loucos e a loucura dos sábios. - Samuel Johnson </h1>
				
			
			</div>
		 
			
		</div>
		
		
<!--=======================================================================================================================================-->
		
		
		<div id="rodape">
			
			<p id="campos">
				<a href="termosDeServicos.php" id="i"> Termos de Serviço </a> //
					
				<a href="referenciasBibliográfica.php" id="i"> Referencias Bibliográfica </a>
			</p>
				
			<footer>
				
				<p> Copyright &copy; 2020 - by João Ferreira <br/>
				<a href="https://www.facebook.com/profile.php?id=100013593917417" target="_blank" id="facebook"> Facebook</a> |
				
				<a href="https://www.instagram.com/joao.ferreira9899" target="_blank" id="instagram"> Instagram </a>
				</p>
				
			</footer>
			
		</div>
		
<!--=======================================================================================================================================-->

	</body>


</html>




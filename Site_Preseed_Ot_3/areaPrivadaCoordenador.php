<?php
	include_once("classes/conexao.php");
	
	session_start();
	if(!isset($_SESSION['id_login'])){
		header("location: login.php");
		exit;
	}else{
		global $conn;
		$sql = "select nivelDeAceso from logonn where id_login = ".$_SESSION['id_login'].";";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			$dados = mysqli_fetch_array($resultado);
			
			if( $dados[0]!="Coordenador"){
				echo"<script language='javascript' type='text/javascript'> 
						alert('Acesso Negado!');
						window.history.back();
					</script>";
			}
		}else {
			echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (01)');
				window.history.back();
			</script>";
		}
	}
	
	
?>


<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Menu do Usuario </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel="stylesheet" href="_css/areaPrivada.css">
		
	</header>
	
	
	<body>

		<div id="cabecalho"> 
			<header id = "cabecalho">
				<nav id = "menu">
					<ul type = "default" >
						<li> <a href ="	sair.php"> Sair </a> </li>
					</ul>
				</nav>
				
				<img id="icone" src = "_imagens/logo2.png"/>
				
			</header>
		</div>
		
		
		<section id="principal">
			
			<h1> Seja Bem Vindo(a)! </h1>
			
			<p id="priv">
				<a href="lancarQuestoes.php" target="_blank"> <input type="submit" name="a" value="Lançar Questões!"/> </a>
				
				<a href="questoesLancadas.php" target="_blank"> <input type="submit" name="a" value="Questões já Lançadas!"/> </a>
			</p>
			
			
			<p id="priv">
			
				<a href="gerarSimulado.php" target="_blank"> <input type="submit" name="a" value="Gerar Simulado!"/> </a>
				
				<a href="abrirFechar_simulado.php" target="_blank"> <input type="submit" name="a" value="Abrir/Fechar Simulado!"/> </a>
				
				<a href="cadRemovProfessor.php" target="_blank"> <input type="submit" name="a" value="Cadastrar/Remover Professor!"/> </a>
								
				<a href="questoesLancadasGeral.php" target="_blank"> <input type="submit" name="a" value="Questões Lançadas Membros!"/> </a>
				
				<a href="sobrePolo.php" target="_blank"> <input type="submit" name="a" value="Sobre o Polo!"/> </a>
				
			</p>
			
			<p id="priv">
				<a href="escolhaSimulado2.php" target="_blank"> <input type="submit" name="a" value="Escolha Simulado!"/> </a> 
				<a href="estatisticaSimResolvidos.php"> <input type="submit" name="a" value="Estatisticas!"/> </a>
			</p>
			
			
		</section>
		
		
		
		<aside id="usuario">
		
			<fieldset id="infoUsuario"> <legend> &nbsp; Usuario &nbsp;</legend>
				
				<?php
				
					if ($conn){
						$sql = "select * from usuario where id_login = '".$_SESSION['id_login']."';";
						$resultado = mysqli_query($conn, $sql);
						
						if ($resultado) {
							if( ($dados = mysqli_fetch_array($resultado)) != null){
								echo " <p> $dados[2] </p> 
									   <p> $dados[3] </p> 
									   <p> $dados[4] </p>";
								
								
								$_SESSION['id_usuario'] = $dados[0];
							}else{
								echo "<script language='javascript' type='text/javascript'>
										alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 04');
										window.location.href='sair.php';
									</script>";
							}
							
						}else {
							echo "<script language='javascript' type='text/javascript'>
								alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 03');
								window.location.href='sair.php';
							</script>";
						}
						
					}else{
						echo "<script language='javascript' type='text/javascript'>
								alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 02');
								window.location.href='sair.php';
							</script>";
					}
				?>
			</fieldset>
			
			<input type="checkbox" id="btn-menu"/>
			<label for="btn-menu"> Editar Perfil! </label>
			
			<nav class="menu">
				<ul type = "default" ><!-- start = "default"> para ol-->
					<li> <a href ="edDadosUsuario.php">  Editar Usuario </a> </li>
					
					<li> <a href ="edLogin.php"> Editar Login </a> </li>
					
					<li> <a href ="edSenha.php"> Editar Senha </a> </li>
				</ul>
			</nav>
			
		</aside>
		
		
		
	</body>
	
	
</html>
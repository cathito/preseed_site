<?php
	
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}
?>


<!DOCTYPE html>

<html lang = "pt-br">

	<head> 
		<meta charset = "UTF-8"/>
		<title> Simulando </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel = "stylesheet" href = "_css/simulados.css"/>
	</head>
<!--==================================================================-->

	<body>
		<div id="cabecalho"> 
			<header id = "cabecalho">
				<nav id = "menu">
					<ul type = "default" ><!-- start = "default"> para ol-->
						<li> <a href ="javascript:window.close();"> < Voltar </a> </li>
					</ul>
				</nav>
				
				<img id="icone" src = "_imagens/logo2.png"/>
				
			</header>
		</div>
		
		<div id="interface"> 
			
			<section id="corpo-full">
			
				<p id="apresentacao"> Tenha Acesso a Todos os Nossos Simulados! </p>
				
				
				<article id="simuladosAbertos">
				
					<fieldset id="simAberto"> <legend>&nbsp; Simulados Em Aberto &nbsp; </legend> 
					
						<?php
							include_once("classes/conexao.php");
						
							$sql = "select * from simuladosLancados where emAberto=1;";
							$buscaSimulado = mysqli_query($conn, $sql);
							
							if($buscaSimulado){
								$contSimuladosAbertos=0;
								
								While($dados = mysqli_fetch_assoc($buscaSimulado)){
									$contSimuladosAbertos++;
									
									echo "<form method='POST' action='resolverSimulado.php'>
											<section id='mostrarSimuldo'>
												<p> Simulado (".$dados["id_simuladoLancado"].") - ".$dados["dataDaPublicacao"]." 
													<br/>".$dados["descrissao"]."
												</p> 
											</section>
											
											<aside id='mostrarSimuldo'> 
												<input type='submit' value='Comecar' id='comecar' name='comecar'>
												<input type='hidden' value='".$dados["id_simuladoLancado"]."' name='verivicador'>
											</aside> 
										</form>";
									
								}
								if($contSimuladosAbertos==0){
									echo "<p> Os Referidos Simulados Não Foram Publicados! </p>";
								}
							}else{
								echo "<script language='javascript' type='text/javascript'>
									alert('Comportamento Inesperado!\nPedimos Desculpas Pelo Transtorno!');
									window.location.href='../areaPrivada.php';
								</script>";
							}
							
						?>
					
					</fieldset>
					
				</article>
				
				
				
				<article id="simuladosAnteriores">
					<fieldset id="simAnterior"> <legend>&nbsp; Simulados Anteriores &nbsp; </legend> 
						<?php
						
							$sql = "select * from simuladosLancados where emAberto=0;";
							$buscaSimulado = mysqli_query($conn, $sql);
							
							if($buscaSimulado){
								$contSimuladosFechados=0;
								
								While($dados = mysqli_fetch_assoc($buscaSimulado)){
									$contSimuladosFechados++;
									echo "<form method='POST' action='visitarSimulado.php'>
											<section id='mostrarSimuldo'>
												<p> Simulado (".$dados["id_simuladoLancado"].") - ".$dados["dataDaPublicacao"]."
													<br/>".$dados["descrissao"]."
												</p>
											</section>
											
											<aside id='mostrarSimuldo'>
												<input type='submit' value='Visitar' id='visitar' name='visitar'>
												<input type='hidden' value='".$dados["id_simuladoLancado"]."' name='id_simuladoLancado'>
											</aside> 
										</form>";
								}
								
								if($contSimuladosFechados==0){
									echo "<p> Os Referidos Simulados Não Foram Publicados! </p>";
								}
							}else{
								echo "<script language='javascript' type='text/javascript'>
									alert('Comportamento Inesperado!\nPedimos Desculpas Pelo Transtorno!');
									window.location.href='../areaPrivada.php';
								</script>";
							}
							
						?>
					</fieldset>
					
				</article>
				
				
			</section>
			
		<!--========================================================-->
		
		</div>
		
	</body>


</html>
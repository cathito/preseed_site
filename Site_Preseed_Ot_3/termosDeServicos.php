<!DOCTYPE html>

<html lang = "pt-br">

	<head> 
		<meta charset = "UTF-8"/>
		<title> Simulando </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel = "stylesheet" href = "_css/termoServico.css"/>
	</head>
<!--==================================================================-->

	<body>
	
		<header id = "cabecalho">
			<nav id = "menu">
				<ul type = "default" ><!-- start = "default"> para ol-->
					<li> <a href ="	index.php"> Página Inicial </a> </li>
					
					<li> <a href ="login.php"> Login </a> </li>
					
					<li> <a href ="escolhaSimulado.php"> Simulados </a> </li>
					
					<li> <a href ="faleConosco.php"> Fale conosco </a> </li>
				</ul>
			</nav>
			
			<img id="icone" src = "_imagens/logo2.png"/>
			
		</header>
		
		
		<div id="corp">
			<h1> Termos de Serviço </h1>
			
			<p> </p>
			
			<fieldset> <legend> &nbsp; CONCORDÂNCIA COM OS TERMOS DE SERVIÇOS &nbsp; </legend>
				
				<p> Ao utilizar os nossos serviços você concorda com todos os termos de serviços. Além de respeitar todas as leis regentes.</p>
				
			</fieldset>
			
			
			<fieldset> <legend> &nbsp; PROJETO BETA &nbsp; </legend>
			
				<p> Este projeto está na fase Beta, quer dizer que ainda não foi concluído, mas é possível de usar antes de haver a liberação do programa. </p>
				
				<p> Por estar na fase Beta a aplicação poderá ser interrompida a qualquer momento sem aviso prévio, visando a segurança dos nossos clientes, realizando assim as correções necessárias.</p> 
				
				<p> Após cada Atualização do projeto Beta todos os dados fornecidos a aplicaçao(dados cadastrais) serão apagados. Consequentemente para acessar nossos serviços deverá ser realizado um novo cadastro. </p>
				
			</fieldset>
			
			
			
			
			<fieldset> <legend> &nbsp; CONDUTA DO USUARIO &nbsp; </legend>
			 
				<p>
					<p> Você entende que todas as informações, dados, texto, fotografias, gráficos, mensagens, tags ou outros materiais, publicados publicamente ou transmitidos em particular, são de responsabilidade exclusiva da pessoa, de quem esse Conteúdo se originou. Isso significa que você, e não o CathitoProgrammer, é totalmente responsável por todo o conteúdo que você enviar, publicar, enviar por e-mail, transmitir ou disponibilizar por meio do Serviço. </p>
					
					<p> O CathitoProgrammer não controla o Conteúdo postado através do Serviço e, como tal, não garante a precisão, a integridade ou a qualidade desse Conteúdo. Você entende que, ao usar o Serviço, pode ser exposto a Conteúdo ofensivo, pornográfico, indecente ou censurável. Sob nenhuma circunstância a CathitoProgrammer será responsável de qualquer forma por qualquer Conteúdo, incluindo, entre outros.</p>

					<p> Você concorda em não usar o Serviço para: </p>
					
					<p> (01) - publicar, enviar por e-mail, transmitir ou disponibilizar qualquer Conteúdo que seja ilegal, prejudicial, ameaçador, abusivo, hostil, tortuoso, difamatório, vulgar, obsceno, difamatório, invasivo da privacidade de outra pessoa, odioso ou racial, étnico ou de outra forma censurável; </p>
					<p> (02) - publicar qualquer conteúdo de natureza é/ou cunho pornográfica. </p>
					<p> (03) - prejudicar pessoas físicas é/ou jurídicas de qualquer forma; </p>
					
				</p>
			</fieldset>
			
			
			<fieldset> <legend> &nbsp; CONTA, SENHA E SEGURANÇA DO USUARIO  &nbsp; </legend>
				<p>
					<p> Ao se cadastar em nossos serviços o usuário fornecera uma designação de conta(Login) e uma senha, sendo o responsável por manter a confidencialidade da senha e da conta e é totalmente responsável por todas as atividades que ocorram sob sua senha ou conta.</p>

					<p> O usuário deve certificar-se de sair da sua conta no final de cada sessão. O CathitoProgrammer não pode e não será responsável por qualquer perda ou dano decorrente do seu não cumprimento desta Seção.</p>
				</p>
			</fieldset>
			
			
			<fieldset> <legend> &nbsp; CUSTO PELO USO DO SERVIÇO &nbsp; </legend>
			
				<p> O nosso serviço é totalmente gratuito. </p>
				
			</fieldset>
			
			
			<fieldset> <legend> &nbsp; SEM REVENDA DE SERVIÇO &nbsp; </legend>
			
				<p> Você concorda em não vender, negociar, revender ou explorar para quaisquer fins comerciais, qualquer parte do Serviço, uso do Serviço ou acesso ao Serviço.</p>
				
			</fieldset>
			
			
			<fieldset> <legend> &nbsp; ESTABILIDADE DO SERVIÇO FORNECIDO &nbsp; </legend>
				
				<p> 
					<p> Os nossos serviços estão limitados aos recursos oferecidos e mantidos pelo Hostinger, consequentemente quaisquer ação é/ou interferência nos serviços e recursos do Hostinger ira nos afetar diretamente.</p>
					
					<p> Em caso de eventuais problemas relacionados diretamente ao código da aplicação, o CathitoProgrammer estara dando a manutenção necessária a aplicação. </p>
					
					<p>
						Vale ressaltar que de tempos em tempos estaremos realizando manutenções as quais irão interromper os nossos serviços por um breve momento.
					</p>
					
				</p>
				
			</fieldset>
			 
			
			<fieldset> <legend> &nbsp; QUEM PODERÁ ACESSAR NOSSOS SERVIÇOS &nbsp; </legend>
			
				<p> De modo geral todos os indivíduos que buscam testar seus conhecimento. </p>
				
				<p> Porem para as organizações que buscam publicar seus conteúdos, se faz necessário a solicitação de tal nível de acesso, que poderá ser solicitado pelo <a href="faleConosco.php" id="i"> Fale Conosco</a>, para que assim tal pedido seja analisado.</p>
				
			</fieldset>
			
		</div>
		
<!--=======================================================================================================================================-->
		
		
		<div id="rodape">
			
			<p id="campos">
				<a href="termosDeServicos.php" id="i"> Termos de Serviço </a> //
					
				<a href="referenciasBibliográfica.php" id="i"> Referencias Bibliográfica </a>
			</p>
			
			<footer>
				
				<p> Copyright &copy; 2020 - by João Ferreira <br/>
				<a href="https://www.facebook.com/profile.php?id=100013593917417" target="_blank" id="facebook"> Facebook</a> |
				
				<a href="https://www.instagram.com/joao.ferreira9899" target="_blank" id="instagram"> Instagram </a>
				</p>
				
			</footer>
			
		</div>
		
<!--=======================================================================================================================================-->
		
		
	</body>


</html>
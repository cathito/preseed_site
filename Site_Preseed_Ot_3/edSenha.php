<?php

	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}
	
?>



<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Simulando Edit </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel="stylesheet" href="_css/t_login.css">
		
	</header>
	
	
	<body>
		
		<header id = "cabecalho">
			<nav id = "menu">
				<ul type = "default" ><!-- start = "default"> para ol-->
					<li> <a href ="javascript:window.history.back();"> < Voltar </a> </li>
				</ul>
			</nav>
			
			<img id="icone" src = "_imagens/logo2.png"/>
			
		</header>
	
<!--=======================================================================================================================================-->
		
		<img id="inicio" src = "_imagens/login.png"/>
		
		
		<div id="corpo-form">
			<form method="POST" action="classes/upSenha.php">
		
				<h1> Editar Login </h1>
				
				<input type="password" placeholder="Nova Senha" name="novaSenha" maxlength="12"/>
				<br/>
				<input type="password" placeholder="Senha Atual" name="senha" maxlength="12"/>
				
				
				<input type="submit" value="Finalizar Edição" id="finalizar" name="finalizar"/>
			</form>
			
		</div>
		
	</body>
	
	
</html>
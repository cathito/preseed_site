<?php
	include_once("classes/conexao.php");
	
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}else{
		global $conn;
		$sql = "select nivelDeAceso from logonn where id_login = ".$_SESSION['id_login'].";";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			$dados = mysqli_fetch_array($resultado);
			
			if( $dados[0]!="Professor" && $dados[0]!="Coordenador" && $dados[0]!="Master"){
				echo"<script language='javascript' type='text/javascript'> 
						alert('Acesso Negado!');
						window.history.back();
					</script>";
			}
		}else {
			echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (01)');
				window.history.back();
			</script>";
		}
	}
/*=======================================================================================================*/
	
	$idQuestao = htmlspecialchars(addslashes($_POST['verivicador']));
	
	$editarQuestao = htmlspecialchars(addslashes($_POST['editarQuestao']));
	
	$questao;
	
/*---------------------------------------*/
	
	if(isset($editarQuestao)) {
		buscarDados($idQuestao);
	}

/*=======================================================================================================*/

	function buscarDados($idQuestao){
		global $conn;
		global $questao;
		
		$sql = "select * from questaoLancada where id_questao = ".$idQuestao.";";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			if(($questao = mysqli_fetch_array($resultado)) != null){
				return true;					
			}
		}else{
			echo "<script >
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (01)');
					window.history.back();
				</script>";
		}
		return false;
	}
	
/*=======================================================================================================*/
	
?>

<html lang = "pt-br">

	<header>
		<meta charset="UTF-8">
		<title> Simulando </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel="stylesheet" href="_css/lancarQuestao.css"> 
		
	</header>
	
<!--==============================================================================-->


	<body>
		<div id="cabecalho"> 
			<header id = "cabecalho">
				<nav id = "menu">
					<ul type = "default" ><!-- start = "default"> para ol-->
						<li> <a href ="javascript:window.close();"> < Voltar </a> </li>
					</ul>
				</nav>
				
				<img id="icone" src = "_imagens/logo2.png"/>
				
			</header>
		</div>
		
		
		<div id="corpo-form">
			<form method="POST" action="classes/ed_Questao.php">
				<h1> Editando Questão! </h1>
				
				<fieldset id="informativo"> <legend> &nbsp; Informativos &nbsp;</legend>
					<p>
						Area Do Conhecimento: 
						<select name="areaConhecimento">
							
							<option value="<?php global $questao; echo $questao[4]; ?>"> <?php global $questao; echo $questao[4]; ?> </option>
							
							<option value=""> ---------- </option>
							<option value="Ciências Humanas e suas Tecnologias"> -- Ciências Humanas e suas Tecnologias -- </option>
							<option value="Ciências da Natureza e suas Tecnologias"> -- Ciências da Natureza e suas Tecnologias -- </option>
							<option value="Linguagens, Códigos e suas Tecnologias"> -- Linguagens, Códigos e suas Tecnologias -- </option>
							<option value="Matemática e suas Tecnologias"> -- Matemática e suas Tecnologias -- </option>
						</select>
						
						<p>
							Alternativa Correta: 
							<select name="alternativaCorreta" id="alternativaCorreta">
								<option value="<?php global $questao; echo $questao[5]; ?>"> <?php global $questao; echo $questao[5]; ?> </option>
							
								<option value=""> ---------- </option>
								<option value="A"> -- A -- </option>
								<option value="B"> -- B -- </option>
								<option value="C"> -- C -- </option>
								<option value="D"> -- D -- </option>
								<option value="E"> -- E -- </option>
							</select>
							
							Nivel de Dificuldade:
							<select name="nivelDificuldade" id="alternativaCorreta">
								<option value="<?php global $questao; echo $questao[6]; ?>"> <?php global $questao; echo $questao[6]; ?> </option>
							
								<option value=""> ---------- </option>
								<option value="Facil"> -- Facil -- </option>
								<option value="Media"> -- Media  -- </option>
								<option value="Dificil"> -- Dificil -- </option>
							</select>
						</p>
					
					</p>
				</fieldset>
				
				
				<fieldset id="questao"> <legend> &nbsp; Questão &nbsp;</legend>
					<p> <textarea name="pergunta" id="cPerg" rows="10"> <?php global $questao; echo $questao[7]; ?> </textarea>  </p>
					
					<p> <textarea name="respostaA" id="cResA" rows="4"> <?php global $questao; echo $questao[8]; ?> </textarea> </p>
					<p> <textarea name="respostaB" id="cResB" rows="4"> <?php global $questao; echo $questao[9]; ?> </textarea> </p>
					<p> <textarea name="respostaC" id="cResC" rows="4"> <?php global $questao; echo $questao[10]; ?> </textarea> </p>
					<p> <textarea name="respostaD" id="cResD" rows="4"> <?php global $questao; echo $questao[11]; ?> </textarea> </p>
					<p> <textarea name="respostaE" id="cResE" rows="4"> <?php global $questao; echo $questao[12]; ?> </textarea> </p>
					
				</fieldset>
				
				<?php global $idQuestao;
					echo "<input type='hidden' name='id_questao' value='".$idQuestao."'/>";
				?>
				
				<input type="submit" name="gravarQuestao" id="editarPerfil" value="Gravar Questão!"/>
				
				<p> - </p>
			</form>
			
		</div>
		
		
		
		
	</body>
	
	
</html>
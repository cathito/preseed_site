<?php
	
	include_once("classes/conexao.php");
	
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}else{
		global $conn;
		$sql = "select nivelDeAceso from logonn where id_login = ".$_SESSION['id_login'].";";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			$dados = mysqli_fetch_array($resultado);
			
			if( $dados[0]!="Professor" && $dados[0]!="Coordenador" && $dados[0]!="Master"){
				echo"<script language='javascript' type='text/javascript'> 
						alert('Acesso Negado!');
						window.history.back();
					</script>";
			}
		}else {
			echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (01)');
				window.history.back();
			</script>";
		}
	}
/*=====================================================================================================*/
	
	$polo;
	$n_Estudantes;
	$simulado;
/*=====================================================================================================*/
	
	
	function polo(){ 
		global $conn;
		global $polo;
		
		$sql = "select usuario.id_polo, polo.alocacao, polo.cidade, polo.estado 
			from usuario JOIN polo on usuario.id_polo = polo.id_polo where id_usuario = ".$_SESSION['id_usuario'].";";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			$polo= mysqli_fetch_array($resultado);
			return true;
		}else{
			echo "<script >
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (02)');
					window.history.back();
				</script>";
		}
		return false;
	}
	
/*========================================================================================================*/

	
	function contarEstudantes(){ 
		global $conn;
		global $polo;
		global $n_Estudantes;
		
		
		$sql = "select count(usuario.id_usuario) from usuario join polo on usuario.id_polo=polo.id_polo where polo.id_polo = ".$polo[0].";";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			$n_Estudantes = mysqli_fetch_array($resultado);
			return true;
		}else{
			echo "<script >
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (03)');
					window.history.back();
				</script>";
		}
		return false;
	}
	
/*========================================================================================================*/
	
?>


<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Menu do Usuario </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel="stylesheet" href="_css/sobre_polo.css"> 
		
		<script>
			function imprimir(){
				var elem = document.getElementById("voltar");
				var elem2 = document.getElementById("imprimir");
				
				elem.style.visibility = "hidden"; 
				elem2.style.visibility = "hidden"; 
				
				window.print();
				
				elem.style.visibility = "visible";
				elem2.style.visibility = "visible";
			
			}
		</script>
	</header>
	
	
	<body>

		<div id="cabecalho"> 
			<header id = "cabecalho">
				<nav id = "menu">
					<ul type = "default" >
						<li id="voltar"> <a href ="javascript:window.close();"> < Voltar </a> </li>
					</ul>
				</nav>
				
				<img id="icone" src = "_imagens/logo2.png"/>
				
			</header>
		</div>
		
		
		<div id= "desenvolvimento"> 
			<?php
				if(polo()){
					if(contarEstudantes()){
						
						echo "<h1> - ".$polo[1]." - ".$polo[2]." - </h1>";
						
						
						$sql="select SL.id_simuladoLancado, SL.dataDaPublicacao, SL.descrissao from simuladoLancado SL where Sl.id_polo = ".$polo[0].";";
						$resultado = mysqli_query($conn, $sql);
						
						if ($resultado){
							While($simulado = mysqli_fetch_assoc($resultado)){
								
								$sql ="select count(simuladorealizado.id_usuario) 
									from simuladorealizado join usuario 
									on simuladorealizado.id_usuario = usuario.id_usuario
									where usuario.id_polo = ".$polo[0]." and simuladorealizado.id_simuladoLancado= ".$simulado['id_simuladoLancado'].";";
									
								$resultado = mysqli_query($conn, $sql);
								
								
								if ($resultado) {
									$contador = mysqli_fetch_array($resultado);
									echo "<table id='tabelaspec'>
										
											<caption> Simulado ".$simulado['id_simuladoLancado']." -- ".$simulado['descrissao']." - ".$simulado['dataDaPublicacao']."</caption>
											<tr> <td class='ce'> O Polo Possui </td> <td class='cd'> ".$n_Estudantes[0]." Integrantes Catalogados </td></tr>
											
											<tr> <td rowspan='2' class='ce'> Dos Quais: </td> <td class='cd'>  ".$contador[0]." Resolveram o Simulado</td> </tr>
																							  <tr> <td class='cd'> ".($n_Estudantes[0] - $contador[0])." Não Resolveram o Simulado </td> </tr>
										</table> <br/> 
										
									<p id='barraDivisoria'> </p>";
								}else{
									echo "<script >
											alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (04)');
											window.history.back();
										</script>";
								}
								
							
							}
							
						}else{
							echo "<script >
									alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (04)');
									window.history.back();
								</script>";
						}
						
					}
				}
				
			?>
			
			<br/>
			
			<button onclick="imprimir()" id="imprimir"> Imprimir Relatorio </button>
			
			<br/>
			
			
		</div>
		
	</body>
	
	
</html>
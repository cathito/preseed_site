<?php
	include_once("classes/conexao.php");
	
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}else{
		global $conn;
		$sql = "select nivelDeAceso from logonn where id_login = ".$_SESSION['id_login'].";";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			$dados = mysqli_fetch_array($resultado);
			
			if( $dados[0]!="Master"){
				echo"<script language='javascript' type='text/javascript'> 
						alert('Acesso Negado!');
						window.history.back();
					</script>";
			}
		}else {
			echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (01)');
				window.history.back();
			</script>";
		}
	}
	
	
?>

<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Menu do Usuario </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel="stylesheet" href="_css/t_login.css"/>
		
	</header>
	
	
	<body>
		<header id = "cabecalho">
			<nav id = "menu">
				<ul type = "default" >
					<li id="voltar"> <a href ="javascript:window.close();"> < Voltar </a> </li>
				</ul>
			</nav>
			
			<img id="icone" src = "_imagens/logo2.png"/>
			
		</header>
		
		<form method="POST" action="classes/class_CadPolo.php">
			
				<h1> Cadastre-Se </h1>
				
				<input type="text" placeholder="Alocação" name="alocacao" maxlength="50"/>
				<input type="email" placeholder="Email Polo" name="email" maxlength="50"/>
				<select name="redeEnsino" >
					<option value="" > Selecinar Rede de Ensino </option>
					<option value="true"> Rede Publica </option>
					<option value="false"> Rede Privada </option>
				</select> 
				
				
				<input type="text" placeholder="Cidade" name="cidade" maxlength="30"/>
				
				<select name="estado" >
					<option value="" > Selecionar Estado </option>
					<option value="AC"> AC </option>
					<option value="AL"> AL </option>
					<option value="AP"> AP </option>
					<option value="AM"> AM </option>
					<option value="BA"> BA </option>
					<option value="CE"> CE </option>
					<option value="DF"> DF </option>
					<option value="ES"> ES </option>
					<option value="GO"> GO </option>
					<option value="MA"> MA </option>
					<option value="MT"> MT </option>
					<option value="MS"> MS </option>
					<option value="MG"> MG </option>
					<option value="PA"> PA </option>
					<option value="PB"> PB </option>
					<option value="PR"> PR </option>
					<option value="PE"> PE </option>
					<option value="PI"> PI </option>
					<option value="RJ"> RJ </option>
					<option value="RN"> RN </option>
					<option value="RS"> RS </option>
					<option value="RO"> RO </option>
					<option value="RR"> RR </option>
					<option value="SC"> SC </option>
					<option value="SP"> SP </option>
					<option value="SE"> SE </option>
					<option value="TO"> TO </option>
				</select> 
				
				
				<input type="submit" value="Cadastrar" id="cadastrar" name="cadastrar">
				
			</form>
		
	</body>
	
	
</html>
<!DOCTYPE html>

<html lang = "pt-br">

	<head> 
		<meta charset = "UTF-8"/>
		<title> Simulando </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel = "stylesheet" href = "_css/simulados.css"/>
	</head>
<!--==================================================================-->

	<body>
	
		<header id = "cabecalho">
			<nav id = "menu">
				<ul type = "default" ><!-- start = "default"> para ol-->
					<li> <a href ="	index.php"> Página Inicial </a> </li>
					
					<li> <a href ="login.php"> Login </a> </li>
					
					<li> <a href ="escolhaSimulado.php"> Simulados </a> </li>
					
					<li> <a href ="faleConosco.php"> Fale conosco </a> </li>
				</ul>
			</nav>
			
			<img id="icone" src = "_imagens/logo2.png"/>
			
		</header>
		
	
<!--=======================================================================================================================================-->
		<section id="corpo-full">
		
			<p id="apresentacao"> Aqui Você Encontra <br/>A Linha Temporal dos Nossos Simulados! </p>
			
			
			<article id="simuladosAbertos">
				
				<fieldset id="simAberto"> <legend>&nbsp; Simulados Em Aberto &nbsp; </legend> 
				
					<?php
						include_once("classes/conexao.php");
					
						$sql = "select * from simuladoLancado where emAberto=true and publico=true;";
						$buscaSimulado = mysqli_query($conn, $sql);
						
						if($buscaSimulado){
							$contSimuladosAbertos=0;
							
							While($dados = mysqli_fetch_assoc($buscaSimulado)){
								$contSimuladosAbertos++;
								
								echo "<section id='mostrarSimuldo'>
										<p> Simulado (".$dados["id_simuladoLancado"].") - ".$dados["dataDaPublicacao"]." 
											<br/>".$dados["descrissao"]."
										</p> 
									</section>";
									
							}
							if($contSimuladosAbertos==0){
								echo "<p> Os Referidos Simulados Não Foram Publicados! </p>";
							}
						}else{
							echo "<script language='javascript' type='text/javascript'>
								alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 01');
								window.history.back();
							</script>";
						}
						
					?>
				
				</fieldset>
					
			</article>
				
				
				
			<article id="simuladosAnteriores">
				<fieldset id="simAnterior"> <legend>&nbsp; Simulados Anteriores &nbsp; </legend> 
					<?php
					
						$sql = "select * from simuladoLancado where emAberto=false and publico=true;";
						$buscaSimulado = mysqli_query($conn, $sql);
						
						if($buscaSimulado){
							$contSimuladosFechados=0;
							
							While($dados = mysqli_fetch_assoc($buscaSimulado)){
								$contSimuladosFechados++;
								echo "<section id='mostrarSimuldo'>
										<p> Simulado (".$dados["id_simuladoLancado"].") - ".$dados["dataDaPublicacao"]."
											<br/>".$dados["descrissao"]."
										</p>
									</section>";
							}
							
							if($contSimuladosFechados==0){
								echo "<p> Os Referidos Simulados Não Foram Publicados! </p>";
							}
						}else{
							echo "<script language='javascript' type='text/javascript'>
								alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 02');
								window.history.back();
							</script>";
						}
						
					?>
				</fieldset>
				
			</article>
			
			
		</section>
		
		
<!--=======================================================================================================================================-->

		
		<div id="rodape">
			
			<p id="campos">
				<a href="termosDeServicos.php" id="i"> Termos de Serviço </a> //
					
				<a href="referenciasBibliográfica.php" id="i"> Referencias Bibliográfica </a>
			</p>
			
			<footer>
				
				<p> Copyright &copy; 2020 - by João Ferreira <br/>
				<a href="https://www.facebook.com/profile.php?id=100013593917417" target="_blank" id="facebook"> Facebook</a> |
				
				<a href="https://www.instagram.com/joao.ferreira9899" target="_blank" id="instagram"> Instagram </a>
				</p>
				
			</footer>
			
		</div>
		
<!--=======================================================================================================================================-->
	
	</body>


</html>
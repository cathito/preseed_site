<?php
	
	include_once("classes/conexao.php");
	
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}else{
		global $conn;
		$sql = "select nivelDeAceso from logonn where id_login = ".$_SESSION['id_login'].";";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			$dados = mysqli_fetch_array($resultado);
			
			if( $dados[0]!="Professor" && $dados[0]!="Coordenador" && $dados[0]!="Master"){
				echo"<script language='javascript' type='text/javascript'> 
						alert('Acesso Negado!');
						window.history.back();
					</script>";
			}
		}else {
			echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (01)');
				window.history.back();
			</script>";
		}
	}
/*=====================================================================================================*/
	
	$polo;
	$n_Estudantes;
	$n_Professor;
	$n_Integrantes;
	
/*=====================================================================================================*/
	
	
	function buscar_Id_polo(){ 
		global $conn;
		global $polo;
		
		$sql = "select polo.id_polo, polo.alocacao, polo.cidade, polo.estado
				from usuario join polo 
				on usuario.id_polo = polo.id_polo 
				where usuario.id_usuario = ".$_SESSION['id_usuario'].";";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			$polo= mysqli_fetch_array($resultado);
			return true;
		}else{
			echo "<script >
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (02)');
					window.history.back();
				</script>";
		}
		return false;
	}
	
	
	function contarUsuarios(){ 
		global $conn;
		global $polo;
		global $n_Integrantes;
		
		$sql = "select count(id_usuario) from usuario where id_polo = ".$polo[0].";";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			$n_Integrantes = mysqli_fetch_array($resultado);
			return true;
		}else{
			echo "<script >
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (03)');
					window.history.back();
				</script>";
		}
		return false;
	}
	
	
	function contarEstudantes(){ 
		global $conn;
		global $polo;
		global $n_Estudantes;
		
		
		$sql = "select count(usuario.id_usuario) from usuario join logonn	
				on usuario.id_login = logonn.id_login
				where usuario.id_polo = ".$polo[0]." and logonn.nivelDeAceso='Estudante';";
		
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			$n_Estudantes = mysqli_fetch_array($resultado);
			return true;
		}else{
			echo "<script >
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (04)');
					window.history.back();
				</script>";
		}
		return false;
	}
	
	
	function contarProfessor(){ 
		global $conn;
		global $polo;
		global $n_Professor;
		
		
		$sql = "select count(usuario.id_usuario) from usuario join logonn		
				where usuario.id_polo = ".(int)$polo[0]." and usuario.id_login = logonn.id_login and logonn.nivelDeAceso='Professor';";
		
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			$n_Professor = mysqli_fetch_array($resultado);
			return true;
		}else{
			echo "<script >
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (05)');
					window.history.back();
				</script>";
		}
		return false;
	}
	
?>


<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Menu do Usuario </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel="stylesheet" href="_css/sobre_polo.css">
		<link rel="stylesheet" href="_css/areaPrivada.css">
		
		
		<script>
			function imprimir(){
				var elem = document.getElementById("voltar");
				var elem2 = document.getElementById("imprimir");
				
				elem.style.visibility = "hidden"; 
				elem2.style.visibility = "hidden"; 
				
				window.print();
				
				elem.style.visibility = "visible";
				elem2.style.visibility = "visible";
			
			}
		</script>
	</header>
	
	
	<body>

		<div id="cabecalho"> 
			<header id = "cabecalho">
				<nav id = "menu">
					<ul type = "default" >
						<li id="voltar"> <a href ="javascript:window.close();"> < Voltar </a> </li>
					</ul>
				</nav>
				
				<img id="icone" src = "_imagens/logo2.png"/>
				
			</header>
		</div>
		
		
		<div> 
			<?php
				if(buscar_Id_polo()){
					if(contarUsuarios()){
						if(contarEstudantes()){
							contarProfessor();
						}
					}
				}
				global $polo;
				global $n_Estudantes;
				global $n_Professor;
				global $n_Integrantes;
				
				echo "<h1> Polo ".$polo[1]." <br/> ".$polo[2]."-".$polo[3]."
					
					<table id='tabelaspec'>
						
						<tr> <td class='ce'> O Polo Possui </td> <td class='cd'> ".$n_Integrantes[0]." Integrantes Catalogados </td></tr>
						
						<tr> <td rowspan='2' class='ce'> Dos Quais: </td> <td class='cd'> ".( $n_Integrantes[0]-($n_Professor[0]+$n_Estudantes[0]) )." Coordenador </td> </tr>
																		  <tr> <td class='cd'> ".$n_Professor[0]." Professor </td> </tr>
						<tr> <td class='ce'> E um Total de </td> <td class='cd'> ".$n_Estudantes[0]." São Alunos </td></tr>
					</table>";
				
			?>
			
			<br/>
			
			<button onclick="imprimir()" id="imprimir"> Imprimir Relatorio </button>
			
			<br/>
			
			
		</div>
		
	</body>
	
	
</html>
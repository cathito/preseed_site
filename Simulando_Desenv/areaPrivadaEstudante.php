<?php
	
	session_start();
	if(!isset($_SESSION['id_login'])){
		header("location: login.php");
		exit;
	}
	
	include_once("classes/conexao.php");
	/*Foto de Tirachard Kumtanom no Pexels*/
?>


<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Menu do Usuario </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel="stylesheet" href="_css/areaPrivada.css">
		
	</header>
	
	
	<body>

		<header id = "cabecalho">
			<nav id = "menu">
				<ul type = "default" >
					<li> <a href ="	sair.php"> Sair </a> </li>
				</ul>
			</nav>
			
			<img id="icone" src = "_imagens/logo2.png"/>
			
		</header>
		
		

		<section id="principal">
			
			<h1> Seja Bem Vindo(a)! </h1>
			
			<p class="botoesUsuario"> 
				<a href="escolhaSimulado2.php" target="_blank"> <input type="submit" name="a" value="Escolha Simulado!"/> </a> <br/>
				<a href="estatisticaSimResolvidos.php"> <input type="submit" name="a2" value="Estatisticas!"/> </a>
			</p>
			
		</section>
		
		
		
		<aside id="usuario">
		
			<fieldset id="infoUsuario"> <legend> &nbsp; Usuario &nbsp;</legend>
				
				<?php
					
					$sql = "select id_usuario, nome, email from usuario where id_login = '".$_SESSION['id_login']."';";
					$resultado = mysqli_query($conn, $sql);
					
					if ($resultado) {
						if( ($dados = mysqli_fetch_array($resultado)) != null){
							echo " <p> $dados[1] </p> 
								   <p> $dados[2] </p>";
							
							$_SESSION['id_usuario'] = $dados[0];
						}else{
							echo "<script language='javascript' type='text/javascript'>
									alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 02');
									window.location.href='sair.php';
								</script>";
						}
						
					}else {
						echo "<script language='javascript' type='text/javascript'>
							alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 01');
							window.location.href='sair.php';
						</script>";
					}
					
				?>
			</fieldset>
			
			<div id="edicao">
			
				<input type="checkbox" id="btn-menu"/>
				<label for="btn-menu"> Editar Perfil! </label>
				
				<nav class="menu">
					<ul type = "default" ><!-- start = "default"> para ol-->
						<li> <a href ="edDadosUsuario.php"> - Usuário - </a> </li>
						
						<li> <a href ="edLogin.php"> - Login - </a> </li>
						
						<li> <a href ="edSenha.php"> - Senha - </a> </li>
					</ul>
				</nav>
			
			</div>
		</aside>
		
		
		
	</body>
	
	
</html>
<?php
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}
	include_once("classes/conexao.php");
?>

<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Simulando </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel="stylesheet" href="_css/estatiSimResolv.css">
		
	</header>
	
	
	<body>
	
		<div id="cabecalho"> 
			<header id = "cabecalho">
				<nav id = "menu">
					<ul type = "default" ><!-- start = "default"> para ol-->
						<li> <a href ="javascript:window.history.back();"> < Voltar </a> </li>
					</ul>
				</nav>
				
				<img id="icone" src = "_imagens/logo2.png"/>
				
			</header>
		</div>
		
		<h2> Veja Sua Desenvoltura </h2>
		
			<?php
				
				$sql = "select SR.id_simuladoLancado, SR.gabaritoUsuario, SR.acertos, SL.descrissao, SL.dataDaPublicacao
						from simuladoRealizado SR join simuladoLancado SL
						on SR.id_simuladoLancado = SL.id_simuladoLancado
						where SR.id_usuario = ".$_SESSION['id_usuario']." order by SR.id_simuladoRealizado desc;";
				
				$resultado_data = mysqli_query($conn, $sql);
				
				if ($resultado_data) {
					$contador=0;
					While($dados = mysqli_fetch_assoc($resultado_data)){
						$contador++;
						echo"
						<fieldset id='maior'> <legend> &nbsp; Simulado (".$dados['id_simuladoLancado'].") -- ".$dados['descrissao']." --".
						date('d/m/Y', strtotime($dados['dataDaPublicacao']))." &nbsp;</legend>
						
							<table id='tabelaspec'>
								<tr> <td class='ce'> Simulado Composto por: </td> <td class='cd'> ".strlen($dados['gabaritoUsuario'])." Questões </td></tr>
								<tr> <td class='ce'> Das Quais Foram Marcadas Corretamente: </td> <td class='cd'> ".$dados['acertos']." Questões </td></tr>
							</table>
						
						</fieldset>";
						
					}
					if($contador<1){
						echo "<div id='n_encontrado'> <h2> O Usuario não Apresenta Simuldaos Resolvidos <br/>e/ou Encontrados em Nossos Cadastros </h2> </div>";
					}
					
				}else {
					echo "<script language='javascript' type='text/javascript'>
							alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 01');
							window.close();
						</script>";
				}
		
				
			?>
		
	</body>
	
	
</html>
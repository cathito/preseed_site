<!DOCTYPE html>

<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Simulando </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel="stylesheet" href="_css/t_login.css"/>
	</header>
	
	
	<body id="cadastro">
	
		<header id = "cabecalho">
			<nav id = "menu">
				<ul type = "default" ><!-- start = "default"> para ol-->
					<li> <a href ="	index.php"> Página Inicial </a> </li>
					
					<li> <a href ="login.php"> Login </a> </li>
					
					<li> <a href ="escolhaSimulado.php"> Simulados </a> </li>
					
					<li> <a href ="faleConosco.php"> Fale conosco </a> </li>
				</ul>
			</nav>
			
			<img id="icone" src = "_imagens/logo2.png"/>
			
		</header>
		
	
<!--=======================================================================================================================================-->
		
		<img id="inicio" src = "_imagens/login.png"/>
		
		
		<div id="corpo-form-Cad">
			<form method="POST" action="classes/cadastro.php">
			
				<h1> Cadastre-Se </h1>
				
				<input type="text" placeholder="Nome Completo" name="nome" maxlength="40"/>
				<input type="email" placeholder="Email" name="email" maxlength="40"/>
				
				<select name="polo" id="cPolo" >
					<option value=""> Selecionar </option>
					
					<?php 
						include_once("classes/conexao.php");
						
						$sql = "select id_polo, alocacao, cidade, estado from polo order by cidade;";
						$resultado = mysqli_query($conn, $sql);
						
						if ($resultado) {
							
							While($dados = mysqli_fetch_array($resultado)){
								echo "<option value='$dados[0]'> Polo $dados[2]-$dados[3] // $dados[1]</option>";	
							}
							echo "<option value='0' > Outros </option>";
							
						}else{
							echo "<script >
									alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (01)');
									window.history.back();
								</script>";
						}
						
					?>
					
				</select> 
				
				<input type="text" placeholder="Login" name="login" maxlength="12"/>
				<input type="password" placeholder="Senha" name="senha" maxlength="12"/>
				
				<input type="submit" value="Cadastrar" id="cadastrar" name="cadastrar">
				
			</form>
			
			<p> <a href="termosDeServicos.php" id="termo"> Termos de Serviço </a> </p>
			
			<!--input type="checkbox" id="termoServico"/> <label for="termoServico"> declaro que li e concordo com os <a href='termosDeServicos.php'> Termos de Serviço </a> </label-->
			
		</div>
		
		
	</body>
	
	
</html>
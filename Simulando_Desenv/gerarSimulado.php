<?php
	include_once("classes/conexao.php");
	
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}else{
		global $conn;
		$sql = "select nivelDeAceso from logonn where id_login = ".$_SESSION['id_login'].";";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			$dados = mysqli_fetch_array($resultado);
			
			if( $dados[0]!="Coordenador" && $dados[0]!="Master"){
				echo"<script language='javascript' type='text/javascript'> 
						alert('Acesso Negado!');
						window.history.back();
					</script>";
			}
		}else {
			echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (01)');
				window.history.back();
			</script>";
		}
	}
/*----------------------------------------------*/
	$id_polo;
	
	$dadosLinguagem;
	$dadosHumanas;
	
	$dadosCiencias;
	$dadosMatematica;
/*============================================================================================================*/

	function buscar__Id_Polo(){
		global $conn;
		global $id_polo;
		
		$sql = "select id_polo from usuario where id_login = ".$_SESSION['id_usuario'].";";
		$resultado = mysqli_query($conn, $sql);
		
		if($resultado) {
			$id_polo= mysqli_fetch_array($resultado);
			return true;
		}else{
			echo "<script >
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (02)');
					window.history.back();
				</script>";
			return false;
		}
	}
	
	
	function linguagens(){
		global $dadosLinguagem;
		global $id_polo;
		global $conn;
		
		$sql = "select count(questaoLancada.id_questao) from questaoLancada, usuario
			where (questaolancada.id_usuario = usuario.id_usuario and usuario.id_polo = ".$id_polo[0].") and 
			areaDoConheciento = 'Linguagens, Códigos e suas Tecnologias' and id_simulado = 0;";
			
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			$dadosLinguagem = mysqli_fetch_array($resultado);
			return true;
		}
		return false;
	}
	
/*============================================================================================================*/	
	
	
	function humanas(){
		global $dadosHumanas;
		global $id_polo;
		global $conn;
		
		$sql = "select count(questaoLancada.id_questao) from questaoLancada, usuario
			where (questaolancada.id_usuario = usuario.id_usuario and usuario.id_polo = ".$id_polo[0].") and 
			areaDoConheciento = 'Ciências Humanas e suas Tecnologias' and id_simulado = 0;";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			$dadosHumanas = mysqli_fetch_array($resultado);
			return true;
		}
		return false;
	}
	
/*============================================================================================================*/	
	
	
	function naturezas(){
		global $dadosCiencias;
		global $id_polo;
		global $conn;
		
		$sql = "select count(questaoLancada.id_questao) from questaoLancada, usuario
			where (questaolancada.id_usuario = usuario.id_usuario and usuario.id_polo = ".$id_polo[0].") and 
			areaDoConheciento = 'Ciências da Natureza e suas Tecnologias' and id_simulado = 0;";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			$dadosCiencias = mysqli_fetch_array($resultado);
			return true;
		}
		return false;
	}
	
/*============================================================================================================*/
	
	
	function matematica(){
		global $dadosMatematica;
		global $id_polo;
		global $conn;
		
		$sql = "select count(questaoLancada.id_questao) from questaoLancada, usuario
			where (questaolancada.id_usuario = usuario.id_usuario and usuario.id_polo = ".$id_polo[0].") and 
			areaDoConheciento = 'Matemática e suas Tecnologias' and id_simulado = 0;";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			$dadosMatematica = mysqli_fetch_array($resultado);
			return true;
		}
		return false;
	}
	
/*============================================================================================================*/


?>


<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Simulando </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel="stylesheet" href="_css/gerarSimulado.css">
		
		<script>
		
			function pegarData(){
				var data = new Date();
				
				var dia = data.getDate();
				var mes     = data.getMonth()+1;          // 0-11 (zero=janeiro)
				var ano    = data.getFullYear();       // 4 dígitos
				
				if(mes<10){
					mes="0"+mes;
				}
				
				var datinha =ano+"-"+mes+"-"+dia;
				document.getElementById("data").value= datinha;
			}
		</script>
		
	</header>
	
	
	<body>
		
		<header id = "cabecalho">
			<nav id = "menu">
				<ul type = "default" ><!-- start = "default"> para ol-->
					<li> <a href ="javascript:window.close();"> < Voltar </a> </li>
				</ul>
			</nav>
			
			<img id="icone" src = "_imagens/logo2.png"/>
			
		</header>
		
	
		<form method="POST" action="classes/gravarSimulado.php">
			
			<h1> Seja Bem Vindo(a)! </h1>
			
			<input type="hidden" name="data" id="data" placeholder="data..." value=""/> 
			
			
			<fieldset id="infoUsuario"> <legend> &nbsp; Caderno (01) &nbsp;</legend>
				
				<?php
				if(buscar__Id_Polo()){
					if(linguagens()){
						if(!humanas()){
							echo "<script language='javascript' type='text/javascript'>
								alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 01.1');
								window.close();
							</script>";
						}
					}else{
						echo "<script language='javascript' type='text/javascript'>
							alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 01');
							window.close();
						</script>";
					}
				}
					global $dadosLinguagem;
					global $dadosHumanas;
					
					echo "<p> 
						Prova de Linguagens, Códigos e Suas Tecnologias. --- Questões Lancadas: ".$dadosLinguagem[0]."<br/>
						Prova de Ciências Humanas e Suas Tecnologias. --- Questões Lancadas: ".$dadosHumanas[0]."
						<input type='hidden' value='".($dadosLinguagem[0]+$dadosHumanas[0])."' name='n_questoes1' >
					</p>";
					
				?>
				<select name="nivelDeAcesso">
					<option value="false"> Acesso Privado </option>
					<option value="true"> Acesso Publico </option>
				</select>
				
				<input type="submit" name="caderno" value="Gerar Caderno(01)" onmousemove="pegarData()"/>
				
			</fieldset>
			
			
			<fieldset id="infoUsuario"> <legend> &nbsp; Caderno (02) &nbsp;</legend>
				
				<?php
					if(naturezas()){
						if(!matematica()){
							echo "<script language='javascript' type='text/javascript'>
								alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 02.1');
								window.close();
							</script>";
						}
					}else{
						echo "<script language='javascript' type='text/javascript'>
							alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 02');
							window.close();
						</script>";
					}
					global $dadosCiencias;
					global $dadosMatematica;
					
					echo "<p> 
						Prova de Ciências da Natureza e Suas Tecnologias. --- Questões Lancadas: ".$dadosCiencias[0]."<br/>
						Prova de Matemática e Suas Tecnologias. --- Questões Lancadas: ".$dadosMatematica[0]."
						<input type='hidden' value='".($dadosCiencias[0]+$dadosMatematica[0])."' name='n_questoes2' >
					</p>";
					
				?>
				
				<select name="nivelDeAcesso">
					<option value="false"> Acesso Privado </option>
					<option value="true"> Acesso Publico </option>
				</select>
				
				<input type="submit" name="caderno" value="Gerar Caderno(02)" onmousemove="pegarData()"/> 
				
				
			</fieldset>
			
			
		</form>
		
	</body>
	
	
</html>
<?php

	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}
	
	include_once("classes/conexao.php");
	
	
	$dadosUsuario;
	
/*-----------------------------------------------------------------------------------------------*/
	
	
	function buscarDadosUsuario(){
		global $conn;
		global $dadosUsuario;
		
		$sql = "select nome, email from usuario where id_usuario = ".$_SESSION['id_usuario'].";";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			$dadosUsuario= mysqli_fetch_array($resultado);
			return true;
		}else{
			echo "<script >
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (02)');
					window.history.back();
				</script>";
		}
		return false;
	
	}
?>


<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Simulando Edit </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel="stylesheet" href="_css/t_login.css"/>
		
	</header>
	
	
	<body>
		
		<header id = "cabecalho">
			<nav id = "menu">
				<ul type = "default" ><!-- start = "default"> para ol-->
					<li> <a href ="javascript:window.history.back();"> < Voltar </a> </li>
				</ul>
			</nav>
			
			<img id="icone" src = "_imagens/logo2.png"/>
			
		</header>
		
		
<!--=======================================================================================================================================-->
		
		<img id="inicio" src = "_imagens/login.png"/>
		
		
		<div id="corpo-form">
			<form method="POST" action="classes/upDadosUsuario.php">
				<h1> Editar Dados </h1>
				
				<?php
					if(buscarDadosUsuario()){
						global $dadosUsuario;
						echo "<input type='text' placeholder='Nome Completo' name='nome' maxlength='40' value='".$dadosUsuario[0]."'/>
							<input type='email' placeholder='Email' name='email' maxlength='40' value='".$dadosUsuario[1]."'/>";
					}
				?>
				
				<br/>
				<input type="password" placeholder="Senha Atual" name="senha" maxlength="12"/>
				
				<input type="submit" value="Finalizar Edição" id="finalizar" name="finalizar"/>
			</form>
		</div>
		
	</body>
	
	
</html>
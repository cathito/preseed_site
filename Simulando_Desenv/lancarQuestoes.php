<?php
	include_once("classes/conexao.php");
	
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}else{
		global $conn;
		$sql = "select nivelDeAceso from logonn where id_login = ".$_SESSION['id_login'].";";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			$dados = mysqli_fetch_array($resultado);
			
			if( $dados[0]!="Professor" && $dados[0]!="Coordenador" && $dados[0]!="Master"){
				echo"<script language='javascript' type='text/javascript'> 
						alert('Acesso Negado!');
						window.history.back();
					</script>";
			}
		}else {
			echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (01)');
				window.history.back();
			</script>";
		}
	}
	
?>

<html lang = "pt-br">

	<header>
		<meta charset="UTF-8">
		<title> Simulando </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel="stylesheet" href="_css/lancarQuestao.css"> 
		<script>
		
			function pegarData(){
				var data = new Date();
				
				var dia = data.getDate();
				var mes     = data.getMonth();          // 0-11 (zero=janeiro)
				var ano    = data.getFullYear();       // 4 dígitos
				mes++;
				if(mes<10){
					mes="0"+mes;
				}
				
				var datinha =ano+"-"+mes+"-"+dia;
				
				document.getElementById("data").value= datinha;
			}
		</script>
		
		
	</header>
	
<!--==============================================================================-->


	<body>
		<div id="cabecalho"> 
			<header id = "cabecalho">
				<nav id = "menu">
					<ul type = "default" ><!-- start = "default"> para ol-->
						<li> <a href ="javascript:window.close();"> < Voltar </a> </li>
					</ul>
				</nav>
				
				<img id="icone" src = "_imagens/logo2.png"/>
				
			</header>
		</div>
		
		
		<div id="corpo-form">
			<form method="POST" action="classes/lancarQuestao.php">
				<h1> Lancando Uma Nova Questão! </h1>
				
				<fieldset id="informativo"> <legend> &nbsp; Informativos &nbsp;</legend>
					<p> 
						Area Do Conhecimento: 
						<select name="areaConhecimento" onclick="pegarData()">
							<option value=""> Selecionar </option>
							<option value="Ciências Humanas e suas Tecnologias"> -- Ciências Humanas e suas Tecnologias -- </option>
							<option value="Ciências da Natureza e suas Tecnologias"> -- Ciências da Natureza e suas Tecnologias -- </option>
							<option value="Linguagens, Códigos e suas Tecnologias"> -- Linguagens, Códigos e suas Tecnologias -- </option>
							<option value="Matemática e suas Tecnologias"> -- Matemática e suas Tecnologias -- </option>
						</select>
						
						<p>
							Alternativa Correta: 
							<select name="alternativaCorreta" id="alternativaCorreta">
								<option value=""> Selecionar </option>
								<option value="A"> -- A -- </option>
								<option value="B"> -- B -- </option>
								<option value="C"> -- C -- </option>
								<option value="D"> -- D -- </option>
								<option value="E"> -- E -- </option>
							</select>
							
							Nivel de Dificuldade:
							<select name="nivelDificuldade" id="alternativaCorreta">
								<option value=""> Selecionar </option>
								<option value="Facil"> -- Facil -- </option>
								<option value="Media"> -- Media  -- </option>
								<option value="Dificil"> -- Dificil -- </option>
							</select>
						</p>
					
					</p>
				</fieldset>
				
				
				<fieldset id="questao"> <legend> &nbsp; Questão &nbsp;</legend>
					<p> <textarea name="pergunta" id="cPerg" rows="10" placeholder="Pergunta..."></textarea>  </p>
					
					<p> <textarea name="respostaA" id="cResA" rows="4" placeholder="Alternativa (A)..."></textarea> </p>
					<p> <textarea name="respostaB" id="cResB" rows="4" placeholder="Alternativa (B)..."></textarea> </p>
					<p> <textarea name="respostaC" id="cResC" rows="4" placeholder="Alternativa (C)..."></textarea> </p>
					<p> <textarea name="respostaD" id="cResD" rows="4" placeholder="Alternativa (D)..."></textarea> </p>
					<p> <textarea name="respostaE" id="cResE" rows="4" placeholder="Alternativa (E)..."></textarea> </p>
					
				</fieldset>
				
				<p> <input type="hidden" name="data" id="data" placeholder="data..." value=""/> </p>
				
				<input type="submit" name="gravarQuestao" id="editarPerfil" value="Gravar Questão!" onmousemove="pegarData()"/>
				
				<p> - </p>
			</form>
			
		</div>
		
		
		
		
	</body>
	
	
</html>
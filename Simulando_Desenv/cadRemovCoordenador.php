<?php
	include_once("classes/conexao.php");
	
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}else{
		global $conn;
		$sql = "select nivelDeAceso from logonn where id_login = ".$_SESSION['id_login'].";";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			$dados = mysqli_fetch_array($resultado);
			
			if( $dados[0]!="Master"){
				echo"<script language='javascript' type='text/javascript'> 
						alert('Acesso Negado!');
						window.history.back();
					</script>";
			}
		}else {
			echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (01)');
				window.history.back();
			</script>";
		}
	}
	
?>

<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Menu do Usuario </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel="stylesheet" href="_css/areaPrivada.css">
		
	</header>
	
	
	<body>

		<div id="cabecalho"> 
			<header id = "cabecalho">
				<nav id = "menu">
					<ul type = "default" >
						<li id="voltar"> <a href ="javascript:window.close();"> < Voltar </a> </li>
					</ul>
				</nav>
				
				<img id="icone" src = "_imagens/logo2.png"/>
				
			</header>
		</div>
		
		<h1 style='text-align:center'> Acesso ao Coordenador </h1>
		
		
		<fieldset id="cadProf"> <legend>&nbsp; Cadastrar Coordenador &nbsp; </legend> 
			<form method="POST" action="classes/class_CadCoordenador.php">
				
				<input type="email" placeholder="Email" name="email" maxlength="40"/>
				
				<br/>
				
				<input type="submit" value="Cadastrar" name="cadastrar">
				
			</form>
			
		</fieldset>
		
		<fieldset id="remProf"> <legend>&nbsp; Remover Coordenador &nbsp; </legend> 
			<form method="POST" action="classes/class_RemCoordenador.php">
				
				<input type="email" placeholder="Email" name="email" maxlength="40"/>
				
				<select name="nivelDeAcesso" >
					<option value=" " > Nivel de Acesso! </option>
					<option value="Estudante"> Estudante </option>
					<option value="Professor"> Professor </option>
				</select> 
				
				<br/>
				
				<input type="submit" value="Remover" name="remover">
				
			</form>
			
		</fieldset>
		
		
	</body>
	
	
</html>

<?php
	include_once("classes/conexao.php");
	
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}else{
		global $conn;
		$sql = "select nivelDeAceso from logonn where id_login = ".$_SESSION['id_login'].";";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			$dados = mysqli_fetch_array($resultado);
			
			if( $dados[0]!="Professor" && $dados[0]!="Coordenador" && $dados[0]!="Master"){
				echo"<script language='javascript' type='text/javascript'> 
						alert('Acesso Negado!');
						window.history.back();
					</script>";
			}
		}else {
			echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (01)');
				window.history.back();
			</script>";
		}
	}
/*======================================================================================================*/
	$id_polo;
	
/*======================================================================================================*/
	function buscar__Id_Polo(){
		global $conn;
		global $id_polo;
		
		$sql = "select id_polo from usuario where id_login = ".$_SESSION['id_usuario'].";";
		$resultado = mysqli_query($conn, $sql);
		
		if($resultado) {
			$id_polo= mysqli_fetch_array($resultado);
			return true;
		}else{
			echo "<script >
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (02)');
					window.history.back();
				</script>";
			return false;
		}
	}
/*======================================================================================================*/
	
?>

<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Simulando </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel="stylesheet" href="_css/questaoLancada.css"> 
		
	</header>
	
	
	<body>
		<div id="cabecalho"> 
			<header id = "cabecalho">
				<nav id = "menu">
					<ul type = "default" ><!-- start = "default"> para ol-->
						<li> <a href ="javascript:window.close();"> < Voltar </a> </li>
					</ul>
				</nav>
				
				<img id="icone" src = "_imagens/logo2.png"/>
				
			</header>
		</div>
	
		<div id="desenvolvimento">
			<?php
				buscar__Id_Polo();
				global $id_polo;
				
				$sql = "select DISTINCT dataDaPublicacao from questaoLancada order by dataDaPublicacao desc;";
				$resultado_data = mysqli_query($conn, $sql);
				
				if ($resultado_data) {
					$contador=0;
					While($row_data = mysqli_fetch_assoc($resultado_data)){
						
						$sql = "select q.areaDoConheciento, q.alternativaCorreta, q.nivelDificuldade, q.pergunta, q.respostaA, q.respostaB, q.respostaC, q.respostaD, q.respostaE, 
							usuario.nome, 
							polo.alocacao, polo.cidade, polo.estado
							from questaoLancada q, usuario, polo 
							where (q.id_usuario = usuario.id_usuario) and (usuario.id_polo=polo.id_polo) and 
							usuario.id_polo = ".$id_polo[0]." and q.dataDaPublicacao = '".$row_data['dataDaPublicacao']."';";
						
						$resultado_questao = mysqli_query($conn, $sql);
						
						if ($resultado_questao){
							
							echo "<fieldset id='maior'> <legend> &nbsp; Questoes lancadas em ".(date('d/m/Y', strtotime($row_data['dataDaPublicacao'])))." &nbsp;</legend>";
							
								While($row_usuario = mysqli_fetch_assoc($resultado_questao)){
									++$contador;
									
									echo "<fieldset id='questao'> <legend>&nbsp; Questao (".$contador.") &nbsp;</legend>
											<p id='informativo'>  
												<strong> Membro: </strong>".$row_usuario['nome']." <br/>
												<strong> Polo: </strong>".$row_usuario['alocacao']." // ".$row_usuario['cidade']."-".$row_usuario['estado']." <br/> <br/>
												
												<strong> -- Informativo -- </strong> <br/>
											   - <strong> Area Do Conheciento: </strong>".$row_usuario['areaDoConheciento']." -<br/>
											   - <strong> Nivel de Dificuldade: </strong> ".$row_usuario['nivelDificuldade']." - //
											   - <strong> Alternativa Correta: </strong> ".$row_usuario['alternativaCorreta']." -
											</p>";
									
									echo " <p>".$row_usuario['pergunta']." </p>
												<label> (A) - ".$row_usuario['respostaA']." </label><br/>
												<label> (B) - ".$row_usuario['respostaB']." </label><br/>
												<label> (C) - ".$row_usuario['respostaC']." </label><br/>
												<label> (D) - ".$row_usuario['respostaD']." </label><br/>
												<label> (E) - ".$row_usuario['respostaE']." </label><br/>
									</fieldset>";
								}
							echo "</fieldset>";
							
						}else {
							echo "<script language='javascript' type='text/javascript'>
									alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 03');
									window.close();
								</script>";
						}
					
					}
					
					if($contador<1){
						echo "<div id='n_encontrado'> <h2> Não há Questões Catalogas Por Nenhum dos Nossos Membros! </h2> </div>";
					}
					
				}else {
					echo "<script language='javascript' type='text/javascript'>
							alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 02');
							window.close();
						</script>";
				}
				
			?>
			
			
		</div>
		
	</body>
	
	
</html>
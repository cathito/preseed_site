<?php
	include_once("classes/conexao.php");
	
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}else{
		global $conn;
		$sql = "select nivelDeAceso from logonn where id_login = ".$_SESSION['id_login'].";";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			$dados = mysqli_fetch_array($resultado);
			
			if( $dados[0]!="Coordenador" && $dados[0]!="Master"){
				echo"<script language='javascript' type='text/javascript'> 
						alert('Acesso Negado!');
						window.close();
					</script>";
			}
		}else {
			echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (01)');
				window.close();
			</script>";
		}
	}
	
/*======================================================================================================*/

	$id_polo;
	
/*======================================================================================================*/
	function buscar__Id_Polo(){
		global $conn;
		global $id_polo;
		
		$sql = "select id_polo from usuario where id_login = ".$_SESSION['id_usuario'].";";
		$resultado = mysqli_query($conn, $sql);
		
		if($resultado) {
			$id_polo= mysqli_fetch_array($resultado);
			return true;
		}else{
			echo "<script >
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (02)');
					window.history.back();
				</script>";
			return false;
		}
	}
/*======================================================================================================*/
?>


<!DOCTYPE html>

<html lang = "pt-br">

	<head> 
		<meta charset = "UTF-8"/>
		<title> Simulando </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel = "stylesheet" href = "_css/simulados.css"/>
	</head>
<!--==================================================================-->

	<body>
	
		<div id="cabecalho"> 
			<header id = "cabecalho">
				<nav id = "menu">
					<ul type = "default" ><!-- start = "default"> para ol-->
						<li> <a href ="javascript:window.close();"> < Voltar </a> </li>
					</ul>
				</nav>
				
				<img id="icone" src = "_imagens/logo2.png"/>
				
			</header>
		</div>
		
		
		<div id="interface"> 
			
			
			<section id="corpo-full">
			
				<p id="apresentacao"> Tenha Acesso a Todos os Nossos Simulados! </p>
				
				
				<article id="simuladosAbertos">
				
					<fieldset id="simAberto"> <legend>&nbsp; Simulados Em Aberto &nbsp; </legend> 
					
						<?php
							buscar__Id_Polo();
							
							$sql = "select id_simuladoLancado, dataDaPublicacao, descrissao, emAberto  from simuladoLancado where id_polo = ".$id_polo[0]." and emAberto = true;";
							$buscaSimulado = mysqli_query($conn, $sql);
							
							if($buscaSimulado){
								$contSimuladosAbertos=0;
								
								While($dados = mysqli_fetch_assoc($buscaSimulado)){
									$contSimuladosAbertos++;
									
									echo "<form method='POST' action='classes/abrirFechar_sim.php'>
											<section id='mostrarSimuldo'>
												<p> Simulado (".$dados["id_simuladoLancado"].") - ".$dados["dataDaPublicacao"]." 
													<br/>".$dados["descrissao"]."
												</p> 
											</section>
											
											<aside id='mostrarSimuldo'> 
												<input type='submit' value='Fechar' name='botao'>
												<input type='hidden' value='".$dados["id_simuladoLancado"]."' name='id_simuladoLancado' id='invisivel'>
												<input type='hidden' value='".$dados["emAberto"]."' name='status' id='invisivel'>
											</aside> 
										</form>";
									
								}
								if($contSimuladosAbertos==0){
									echo "<p> Os Referidos Simulados Não Foram Publicados! </p>";
								}
							}else{
								echo "<script language='javascript' type='text/javascript'>
									alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 03');
									window.close();
								</script>";
							}
							
						?>
					
					</fieldset>
					
				</article>
				
				
				
				<article id="simuladosAnteriores">
					<fieldset id="simAnterior"> <legend>&nbsp; Simulados Fechados &nbsp; </legend> 
					
						<?php
							$sql = "select id_simuladoLancado, dataDaPublicacao, descrissao, emAberto  from simuladoLancado where id_polo = ".$id_polo[0]." and emAberto = 0;";
							$buscaSimulado = mysqli_query($conn, $sql);
							
							if($buscaSimulado){
								$contSimuladosFechados=0;
								
								While($dados = mysqli_fetch_assoc($buscaSimulado)){
									$contSimuladosFechados++;
									echo "<form method='POST' action='classes/abrirFechar_sim.php'>
											<section id='mostrarSimuldo'>
												<p> Simulado (".$dados["id_simuladoLancado"].") - ".$dados["dataDaPublicacao"]."
													<br/>".$dados["descrissao"]."
												</p>
											</section>
											
											<aside id='mostrarSimuldo'>
												<input type='submit' value='Abrir' name='botao'>
												<input type='hidden' value='".$dados["id_simuladoLancado"]."' name='id_simuladoLancado' id='invisivel'>
												<input type='hidden' value='".$dados["emAberto"]."' name='status' id='invisivel'>
											</aside> 
										</form>";
								}
								
								if($contSimuladosFechados==0){
									echo "<p> Os Referidos Simulados Não Foram Publicados! </p>";
								}
							}else{
								echo "<script language='javascript' type='text/javascript'>
									alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno!');
									window.close();
								</script>";
							}
							
						?>
						
					</fieldset>
					
				</article>
				
				
			</section>
			
		<!--========================================================-->
		
		</div>
		
	</body>


</html>
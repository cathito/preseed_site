<?php
	
	include_once("conexao.php");

	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}else{
		global $conn;
		$sql = "select nivelDeAceso from logonn where id_login = ".$_SESSION['id_login'].";";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			$dados = mysqli_fetch_array($resultado);
			
			if($dados[0]!="Coordenador" && $dados[0]!="Master"){
				echo"<script language='javascript' type='text/javascript'> 
						alert('Acesso Negado!');
						window.history.back();
					</script>";
			}
		}else {
			echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (01)');
				window.history.back();
			</script>";
		}
	}
/*============================================================================================================*/

	$email = htmlspecialchars(addslashes($_POST['email']));
	$remover = htmlspecialchars(addslashes($_POST['remover']));
	
	$id_Professor;
 /*---------------------------------------------------------------------*/
	
	if(isset($remover)) {
		if(camposPreenchidos($email)){
			if(buscarLogon($email)){
				baixaProfessor();
			}
		}
		
	}
	
/*============================================================================================================*/
	
	function camposPreenchidos($email){ 
	
		if($email == "" || $email == null) {
			echo "<script >
					alert('Todos os Campos Devem Ser Preenchidos!');
					window.history.back();
				</script>";
			return false;
		}
		return true;
	}
	
/*============================================================================================================*/

	function buscar__Id_Polo(){
		global $conn;
		global $id_polo;
		
		$sql = "select id_polo from usuario where id_login = ".$_SESSION['id_usuario'].";";
		$resultado = mysqli_query($conn, $sql);
		
		if($resultado) {
			$id_polo= mysqli_fetch_array($resultado);
			return true;
		}else{
			echo "<script >
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (02)');
					window.history.back();
				</script>";
			return false;
		}
	}
	
/*============================================================================================================*/
	
	function buscarLogon($email){
		global $conn;
		global $id_Professor;
		global $id_polo;
		
		$sql = "select id_login from usuario where email = '".$email."' and id_polo = ".$id_polo[0].";";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			if(($id_Professor = mysqli_fetch_array($resultado)) != null){
				return true;	
			}else{
				echo "<script >
					alert('Usuario Não Encontrado e/ou Não Catalogado Em Nossos Cadastros!');
					window.history.back();
				</script>";
			}
		}else{
			echo "<script >
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (03)');
					window.history.back();
				</script>";
		}
		
		return false;
	
	}
	
/*============================================================================================================*/
	
	function baixaProfessor(){
		global $conn;
		global $id_Professor;
		
		$sql = "UPDATE logonn SET nivelDeAceso = 'Estudante' WHERE id_login = ".$id_Professor[0].";";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			echo "<script language='javascript' type='text/javascript'>
					alert('Professor Removido com Sucesso!');
					window.close();
				</script>";
		}else {
			echo "<script >
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (03)');
				window.history.back();
			</script>";
		}
	}
	
/*============================================================================================================*/
	
	
	
?>
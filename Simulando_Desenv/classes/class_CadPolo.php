<?php
	
	include_once("conexao.php");

	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}else{
		global $conn;
		$sql = "select nivelDeAceso from logonn where id_login = ".$_SESSION['id_login'].";";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			$dados = mysqli_fetch_array($resultado);
			
			if($dados[0]!="Master"){
				echo"<script language='javascript' type='text/javascript'> 
						alert('Acesso Negado!');
						window.history.back();
					</script>";
			}
		}else {
			echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (01)');
				window.history.back();
			</script>";
		}
	}
/*============================================================================================================*/

	
	$alocacao = htmlspecialchars(addslashes($_POST['alocacao']));
	$email = htmlspecialchars(addslashes($_POST['email']));
	$redeEnsino = htmlspecialchars(addslashes($_POST['redeEnsino']));
	$cidade = htmlspecialchars(addslashes($_POST['cidade']));
	$estado = htmlspecialchars(addslashes($_POST['estado']));
	$cadastrar = htmlspecialchars(addslashes($_POST['cadastrar']));
	
	$id_Professor;
 /*---------------------------------------------------------------------*/
	
	if(isset($cadastrar)) {
		if(camposPreenchidos($alocacao, $email, $cidade, $estado, $redeEnsino)){
			cadastrarPolo($alocacao, $email, $cidade, $estado, $redeEnsino);
		}
		
	}
	
/*============================================================================================================*/
	
	function camposPreenchidos($alocacao, $email, $cidade, $estado, $redeEnsino){ 
	
		if(($alocacao == "" || $alocacao == null) || ($email == "" || $email == null) || ($redeEnsino == "" || $redeEnsino == null) ||
			($cidade == "" || $cidade == null) || ($estado == "" || $estado == null)) {
			echo "<script >
					alert('Todos os Campos Devem Ser Preenchidos!');
					window.history.back();
				</script>";
			return false;
		}
		return true;
	}
	
/*============================================================================================================*/

	
	function cadastrarPolo($alocacao, $email, $cidade, $estado, $redeEnsino){
		global $conn;
		global $id_Professor;
		
		$sql = "INSERT INTO polo VALUES (default,".$redeEnsino.",'".$alocacao."', '".$email."', '". $cidade."', '". $estado."');";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			echo "<script language='javascript' type='text/javascript'>
					alert('Polo Cadastrado com Sucesso!');
					window.close();
				</script>";
		}else {
			echo "<script >
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (02)');
				window.history.back();
			</script>";
		}
	}
	
/*============================================================================================================*/
	
	
	
?>
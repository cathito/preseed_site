<?php
	
	include_once("conexao.php");

	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}else{
		global $conn;
		$sql = "select nivelDeAceso from logonn where id_login = ".$_SESSION['id_login'].";";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			$dados = mysqli_fetch_array($resultado);
			
			if($dados[0]!="Coordenador" && $dados[0]!="Master"){
				echo"<script language='javascript' type='text/javascript'> 
						alert('Acesso Negado!');
						window.history.back();
					</script>";
			}
		}else {
			echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (01)');
				window.history.back();
			</script>";
		}
	}
/*======================================================================================================*/


	$dataDaPublicacao = htmlspecialchars(addslashes($_POST['data']));
	$nivelDeAcesso = htmlspecialchars(addslashes($_POST['nivelDeAcesso']));
	$n_questoesCad1 = htmlspecialchars(addslashes($_POST['n_questoes1']));
	$n_questoesCad2 = htmlspecialchars(addslashes($_POST['n_questoes2']));
	
	$caderno = htmlspecialchars(addslashes($_POST['caderno']));
	

	if($caderno=="Gerar Caderno(01)"){
		$descrissao = "Linguagens // Humanas";
		$areaConhecimento1 = "Linguagens, Códigos e suas Tecnologias";
		$areaConhecimento2 = "Ciências Humanas e suas Tecnologias";
		$n_questoes = $n_questoesCad1;
	}else{
		$descrissao ="Narturezas // Matemática";
		$areaConhecimento1 = "Ciências da Natureza e suas Tecnologias";
		$areaConhecimento2 = "Matemática e suas Tecnologias";
		$n_questoes = $n_questoesCad2;
	}

	$IdSimulado;

/*======================================================================================================*/

	if(isset($caderno)) {
		if( $n_questoes >= 3){
			if(buscar__Id_Polo()){
				if(gerarSimulado($dataDaPublicacao, $descrissao,$id_polo, $nivelDeAcesso)){
					if( buscarIdSimulado($id_polo, $dataDaPublicacao, $descrissao)){
						indicarQuestaoSimulado($id_polo,$areaConhecimento1, $areaConhecimento2);
					}
				}
			}
		}else{
			echo "<script >
				alert('Não é Possivel Gerar Simulado com Menos de 3 Questoes!');
				window.history.back();
			</script>";
		}
	}


/*======================================================================================================*/


	function buscar__Id_Polo(){
		global $conn;
		global $id_polo;
		
		$sql = "select id_polo from usuario where id_login = ".$_SESSION['id_usuario'].";";
		$resultado = mysqli_query($conn, $sql);
		
		if($resultado) {
			$id_polo= mysqli_fetch_array($resultado);
			return true;
		}else{
			echo "<script >
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (02)');
					window.history.back();
				</script>";
			return false;
		}
	}

/*-------------------------------------------------------------------------------------------------------*/

	function gerarSimulado($dataDaPublicacao, $descrissao,$id_polo, $nivelDeAcesso){
		global $conn;
		
		$sql = "insert into simuladoLancado values (default, ".$id_polo[0].", ".$_SESSION['id_usuario'].", '".$dataDaPublicacao."', '".$descrissao."', default, ".$nivelDeAcesso.");";
		$resultado = mysqli_query($conn, $sql);
		
		if($resultado) {
			return true;
		}else{
			echo "<script >
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (03)');
					window.history.back();
				</script>";
			return false;
		}
	}

/*======================================================================================================*/

	function buscarIdSimulado($id_polo, $dataDaPublicacao, $descrissao){
		global $conn;
		global $IdSimulado;
		
		$sql = "select id_simuladoLancado from simuladoLancado 
				where id_polo = ".$id_polo[0]." and id_usuario = ".$_SESSION['id_usuario']." and dataDaPublicacao = '".$dataDaPublicacao."' and descrissao = '".$descrissao."';";
		$resultado = mysqli_query($conn, $sql);
		
		if($resultado){
			$IdSimulado = mysqli_fetch_array($resultado);
			return true;
		}else{
			echo "<script >
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (04)');
					window.history.back();
				</script>";
			return false;
		}
	}

/*-------------------------------------------------------------------------------------------------------*/

	function indicarQuestaoSimulado($id_polo,$areaConhecimento1, $areaConhecimento2){
		global $conn;
		global $IdSimulado;
		
		$sql = "UPDATE questaoLancada, usuario, polo SET questaoLancada.id_simulado = ".$IdSimulado[0]." 
				where questaoLancada.id_usuario = usuario.id_usuario and usuario.id_polo = ".$id_polo[0]." and id_simulado = 0 ";
		
		$resultado = mysqli_query($conn, $sql);
		
		if($resultado){
			echo "<script language='javascript' type='text/javascript'>
				alert('Simulado Gerado Com Sucesso!');
				window.location.href='../gerarSimulado.php';
			</script>";
		}else{
			echo "<script >
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (03)');
					window.history.back();
				</script>";
			return false;
		}
	}


/*======================================================================================================*/

?>
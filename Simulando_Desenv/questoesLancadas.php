
<?php
	include_once("classes/conexao.php");
	
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}else{
		global $conn;
		$sql = "select nivelDeAceso from logonn where id_login = ".$_SESSION['id_login'].";";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			$dados = mysqli_fetch_array($resultado);
			
			if( $dados[0]!="Professor" && $dados[0]!="Coordenador" && $dados[0]!="Master"){
				echo"<script language='javascript' type='text/javascript'> 
						alert('Acesso Negado!');
						window.history.back();
					</script>";
			}
		}else {
			echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (01)');
				window.history.back();
			</script>";
		}
	}
/*======================================================================================================*/
	
?>

<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Simulando </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel="stylesheet" href="_css/questaoLancada.css"> 
		
	</header>
	
	
	<body>
		
		<div id="cabecalho"> 
			<header id = "cabecalho">
				<nav id = "menu">
					<ul type = "default" ><!-- start = "default"> para ol-->
						<li> <a href ="javascript:window.close();"> < Voltar </a> </li>
					</ul>
				</nav>
				
				<img id="icone" src = "_imagens/logo2.png"/>
				
			</header>
		</div>
		
		
		<div id="desenvolvimento">
		
			<?php
				$sql = "select DISTINCT dataDaPublicacao from questaoLancada where id_usuario = ".$_SESSION['id_usuario']." order by dataDaPublicacao desc;";
				$resultado_data = mysqli_query($conn, $sql);
				
				if ($resultado_data) {
					$contador=0;
					
					While($row_data = mysqli_fetch_assoc($resultado_data)){
					
						$sql = "select * from questaoLancada where dataDaPublicacao = '".$row_data['dataDaPublicacao']."' and id_usuario = ".$_SESSION['id_usuario'].";";
						$resultado_questao = mysqli_query($conn, $sql);
						
						if ($resultado_questao) {
				/*==========================================================================================================================================*/
							echo "<fieldset id='maior'> <legend> &nbsp; Questoes lancadas em ".(date('d/m/Y', strtotime($row_data['dataDaPublicacao'])))." &nbsp;</legend>";
							
								While($row_usuario = mysqli_fetch_assoc($resultado_questao)){
									++$contador;
									
									echo "  <fieldset id='questao'> <legend>&nbsp; Questao (".$contador.") &nbsp;</legend> <br/>
									
												<form method='POST' action='editarQuestao.php'>
													<input type='hidden' value='".$row_usuario["id_questao"]."' name='verivicador'>
													<input type='submit' id='editar' value='Editar Questão' name='editarQuestao'>
											
												</form>";
									
									echo "<p id='informativo'>  <strong> -- Informativo -- </strong> <br/>
															   - <strong> Area Do Conheciento: </strong>".$row_usuario['areaDoConheciento']." -<br/>
															   - <strong> Nivel de Dificuldade: </strong> ".$row_usuario['nivelDificuldade']." - //
															   - <strong> Alternativa Correta: </strong> ".$row_usuario['alternativaCorreta']." -
										</p>";
									
									echo " 	<p>".$row_usuario['pergunta']." </p>
											<label> (A) - ".$row_usuario['respostaA']." </label><br/>
											<label> (B) - ".$row_usuario['respostaB']." </label><br/>
											<label> (C) - ".$row_usuario['respostaC']." </label><br/>
											<label> (D) - ".$row_usuario['respostaD']." </label><br/>
											<label> (E) - ".$row_usuario['respostaE']." </label><br/>";
									
									echo "</fieldset>";
								}
								
							echo "</fieldset>";
				/*==========================================================================================================================================*/
				
						}else {
							echo "<script language='javascript' type='text/javascript'>
									alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 03');
									window.close();
								</script>";
						}
					
					}
					
					if($contador<1){
						echo "<div id='n_encontrado'> <h2> O Usuario não Apresenta Questões Catalogas e/ou Encontrados <br/>em Nossos Cadastros </h2> </div>";
					}
					
				}else {
					echo "<script language='javascript' type='text/javascript'>
							alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 02');
							window.close();
						</script>";
				}
		
			?>
			
			
		</div>
		
	</body>
	
	
</html>
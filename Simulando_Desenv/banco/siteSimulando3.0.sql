/*drop database sSimulando;*/

create database sSimulando
default character set utf8
default collate utf8_general_ci;

use sSimulando;

/*================================================================*/

create TABLE logonn ( 
	id_login int not null auto_increment, 
	login VARCHAR(120) NOT NULL, 
	senha VARCHAR(120) NOT NULL,
    nivelDeAceso varchar(12) not null default 'Estudante',
    primary key(id_login) ) 
    default charset=utf8;

create table usuario( 
     id_usuario    int not null auto_increment,
	 id_login    int   not null,
     id_polo   int   not null,
     nome    varchar(52)  not null,
	 email    varchar(120)  not null,
     primary key( id_usuario ) ) 
     default charset=utf8; 

create TABLE polo(
	id_polo int not null auto_increment, 
	redePublica boolean default true,
	alocacao    varchar(50)  not null,
	email    varchar(120)  not null,
	cidade    varchar(30)  not null,
	estado    varchar(2)  not null,
	primary key(id_polo) )
    default charset=utf8;

/*================================================================*/

create table questaoLancada(
	id_questao    int not null auto_increment,
	id_usuario    int   not null,
    id_simulado   int not null,
    dataDaPublicacao   date  not null,
	areaDoConheciento    varchar(40)  not null,
    alternativaCorreta   char not null,
    nivelDificuldade   varchar(10)  not null,
    pergunta   text not null,
    respostaA   text not null,
    respostaB   text not null,
    respostaC   text not null,
    respostaD   text not null,
    respostaE   text not null,
    primary key( id_questao ) ) 
	default charset=utf8; 
    

create TABLE simuladoLancado( 
	id_simuladoLancado int not null auto_increment, 
    id_polo   int   not null,
    id_usuario int not null, 
    dataDaPublicacao   date  not null,
	descrissao VARCHAR(50) not null, 
    emAberto boolean not null default true,
    publico boolean not null default false,
    primary key(id_simuladoLancado) ) 
    default charset=utf8;

create table simuladoRealizado( 
     id_simuladoRealizado    int not null auto_increment,
	 id_usuario    int   not null,
     id_simuladoLancado int   not null,
	 gabaritoUsuario    varchar(100)  not null,
     acertos    int   not null,
     primary key( id_simuladoRealizado ) ) 
     default charset=utf8; 

/*================================================================*/

create TABLE faleConosco ( 
	id_faleConosco int not null auto_increment, 
	nome    varchar(52)  not null,
	email    varchar(120)  not null,
    assunto    varchar(50)  not null,
    mensagem   text not null,
    lida boolean not null default false,
    solucionada boolean not null default false,
    primary key(id_faleConosco) )
    default charset=utf8;
 
 
create TABLE esqueciSenha(
	id_esqueciSenha int not null auto_increment, 
	email    varchar(120)  not null,
	lida boolean not null default false,
	solucionada boolean not null default false,
	primary key(id_esqueciSenha) )
	default charset=utf8;

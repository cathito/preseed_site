
<?php
	
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}
	include_once("classes/conexao.php");
	
	$idSimulado = htmlspecialchars(addslashes($_POST['verivicador']));
	$contador=0;
?>

<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Simulando </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel="stylesheet" href="_css/resolvSimulado.css"> 
		
	</header>
	
	
	<body>
	
		<div id="cabecalho"> 
			<header id = "cabecalho">
				<nav id = "menu">
					<ul type = "default" ><!-- start = "default"> para ol-->
						<li> <a href ="javascript:window.close();"> < Voltar </a> </li>
					</ul>
				</nav>
				
				<img id="icone" src = "_imagens/logo2.png"/>
				
			</header>
		</div>
	
		<div id="a">
			
			<form method="POST" action="resultado.php">
			
				<?php
					
					$sql = "select q.pergunta, q.respostaA, q.respostaB, q.respostaC, q.respostaD, q.respostaE 
						from questaoLancada q where id_simulado = '".$idSimulado."' order by areaDoConheciento asc;";
					$resultado = mysqli_query($conn, $sql);
					
					if ($resultado) {
						
						echo "<fieldset id='maior'> <legend> &nbsp; Simulado (0".$idSimulado.") &nbsp;</legend>";
						
							While($row_usuario = mysqli_fetch_assoc($resultado)){
								++$contador;
								
								echo "<fieldset id='questao'> <legend>&nbsp; Questao (".$contador.") &nbsp;</legend>";
								
								echo " <p>".$row_usuario['pergunta']." </p>";
								echo " <input type='radio' name='Q".$contador."' value='A'/> <label> (A) - ".$row_usuario['respostaA']." </label><br/>";
								echo " <input type='radio' name='Q".$contador."' value='B'/> <label> (B) - ".$row_usuario['respostaB']." </label><br/>";
								echo " <input type='radio' name='Q".$contador."' value='C'/> <label> (C) - ".$row_usuario['respostaC']." </label><br/>";
								echo " <input type='radio' name='Q".$contador."' value='D'/> <label> (D) - ".$row_usuario['respostaD']." </label><br/>";
								echo " <input type='radio' name='Q".$contador."' value='E'/> <label> (E) - ".$row_usuario['respostaE']." </label><br/>";
								echo " <input type='radio' name='Q".$contador."' value='-' id='null'checked/>
								</fieldset>";
							}
						echo "</fieldset>";
							
					}else {
						echo "<script language='javascript' type='text/javascript'>
								alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 01');
								window.history.back();
							</script>";
					}
					
					echo "<input type='hidden' name='contador' value='".$contador."' id = 'invisivel'/>
						<input type='hidden' name='idSimulado' value='".$idSimulado."' id = 'invisivel'/>";
				?>
				
				<input type="submit" id = "finalizar" value="Finalizar Simulado!">
			
			</form>
			
		</div>
		
	</body>
	
	
</html>
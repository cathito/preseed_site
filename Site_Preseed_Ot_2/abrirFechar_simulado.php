<!DOCTYPE html>

<html lang = "pt-br">

	<head> 
		<meta charset = "UTF-8"/>
		<title> Simulando </title>
		<link rel = "stylesheet" href = "_css/simulados.css"/>
	</head>
<!--==================================================================-->

	<body>
		<div id="interface"> 
			
			
			<section id="corpo-full">
			
				<p id="apresentacao"> Tenha Acesso a Todos os Nossos Simulados! </p>
				
				
				<article id="simuladosAbertos">
				
					<fieldset id="simAberto"> <legend>&nbsp; Simulados Em Aberto &nbsp; </legend> 
					
						<?php
							include_once("classes/conexao.php");
						
							$sql = "select * from simuladosLancados where emAberto= true;";
							$buscaSimulado = mysqli_query($conn, $sql);
							
							if($buscaSimulado){
								$contSimuladosAbertos=0;
								
								While($dados = mysqli_fetch_assoc($buscaSimulado)){
									$contSimuladosAbertos++;
									
									echo "<form method='POST' action='classes/abrirFechar_sim.php'>
											<section id='mostrarSimuldo'>
												<p> Simulado (".$dados["id_simuladoLancado"].") - ".$dados["dataDaPublicacao"]." 
													<br/>".$dados["descrissao"]."
												</p> 
											</section>
											
											<aside id='mostrarSimuldo'> 
												<input type='submit' value='Fechar' name='botao'>
												<input type='hidden' value='".$dados["id_simuladoLancado"]."' name='id_simuladoLancado' id='invisivel'>
												<input type='hidden' value='".$dados["emAberto"]."' name='status' id='invisivel'>
											</aside> 
										</form>";
									
								}
								if($contSimuladosAbertos==0){
									echo "<p> Os Referidos Simulados Não Foram Publicados! </p>";
								}
							}else{
								echo "<script language='javascript' type='text/javascript'>
									alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 01');
									window.history.back();
								</script>";
							}
							
						?>
					
					</fieldset>
					
				</article>
				
				
				
				<article id="simuladosAnteriores">
					<fieldset id="simAnterior"> <legend>&nbsp; Simulados Fechados &nbsp; </legend> 
						<?php
						
							$sql = "select * from simuladosLancados where emAberto=0;";
							$buscaSimulado = mysqli_query($conn, $sql);
							
							if($buscaSimulado){
								$contSimuladosFechados=0;
								
								While($dados = mysqli_fetch_assoc($buscaSimulado)){
									$contSimuladosFechados++;
									echo "<form method='POST' action='classes/abrirFechar_sim.php'>
											<section id='mostrarSimuldo'>
												<p> Simulado (".$dados["id_simuladoLancado"].") - ".$dados["dataDaPublicacao"]."
													<br/>".$dados["descrissao"]."
												</p>
											</section>
											
											<aside id='mostrarSimuldo'>
												<input type='submit' value='Abrir' name='botao'>
												<input type='hidden' value='".$dados["id_simuladoLancado"]."' name='id_simuladoLancado' id='invisivel'>
												<input type='hidden' value='".$dados["emAberto"]."' name='status' id='invisivel'>
											</aside> 
										</form>";
								}
								
								if($contSimuladosFechados==0){
									echo "<p> Os Referidos Simulados Não Foram Publicados! </p>";
								}
							}else{
								echo "<script language='javascript' type='text/javascript'>
									alert('Comportamento Inesperado!\nPedimos Desculpas Pelo Transtorno!');
									window.location.href='../areaPrivada.php';
								</script>";
							}
							
						?>
					</fieldset>
					
				</article>
				
				
			</section>
			
		<!--========================================================-->
		
		</div>
		
	</body>


</html>
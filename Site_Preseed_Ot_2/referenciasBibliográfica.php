<!DOCTYPE html>

<html lang = "pt-br">

	<head> 
		<meta charset = "UTF-8"/>
		<title> Simulando </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel = "stylesheet" href = "_css/referencias.css"/>
	</head>
<!--==================================================================-->

	<body>
	
		<header id = "cabecalho">
			<nav id = "menu">
				<ul type = "default" ><!-- start = "default"> para ol-->
					<li> <a href ="	index.php"> Página Inicial </a> </li>
					
					<li> <a href ="login.php"> Login </a> </li>
					
					<li> <a href ="escolhaSimulado.php"> Simulados </a> </li>
					
					<li> <a href ="faleConosco.php"> Fale conosco </a> </li>
				</ul>
			</nav>
			
			<img id="icone" src = "_imagens/logo2.png"/>
			
		</header>
		
		
		<div>
			
			<h1> Referências </h1>
			
			<fieldset>
				<p>
					Imagem de fundo da Tela inicial:<br/>
					<a href="https://www.pexels.com/photo/people-notes-meeting-team-7095/" target="_blank"> Foto de fotografias de inicialização no Pexels </a>
				</p>
			</fieldset>
			
			<br/>
			<fieldset>
				<p>
					Imagem de fundo da: Tela de Login, Tela de Cadastro, Tela Esqueci a Senha,<br/>
					Tela Edção Dados, Tela Edção Login, Tela Edção Senha: <br/>
					<a href="https://www.pexels.com/photo/person-holding-photo-of-single-tree-at-daytime-1252983/" target="_blank"> Foto de Lisa Fotios no Pexels </a>
				</p>
			</fieldset>
			
			<br/>
			<fieldset>
				<p>
					Imagem de fundo da Tela de: Lancar Questão, Questoes Lancadas, Resolução Simulado, Visitar Simulado <br/>
					<a href="https://www.pexels.com/pt-br/foto/alvenaria-azulejo-concreto-conhecimento-220182/" target="_blank"> Foto de Pixabay no Pexels </a>
				</p>
			</fieldset>
			
			
			<br/>
			<fieldset>
				<p>
					Imagem de fundo da Tela de: Tela Principal do Aluno, Professor, Coordenador<br/>
					<a href="https://www.pexels.com/pt-br/foto/ambiente-de-trabalho-area-de-trabalho-aviso-balcao-733857/" target="_blank"> Foto de Tirachard Kumtanom no Pexels</a>
				</p>
			</fieldset>
			
			
		</div>
		
		<p> </p><br/><br/>
		
<!--=======================================================================================================================================-->
		
		
		<div id="rodape">
			
			<p id="campos">
				<a href="termosDeServicos.php" id="i"> Termos de Serviço </a> //
					
				<a href="referenciasBibliográfica.php" id="i"> Referencias Bibliográfica </a>
			</p>
				
			<footer>
				
				<p> Copyright &copy; 2020 - by João Ferreira <br/>
				<a href="https://www.facebook.com/profile.php?id=100013593917417" target="_blank" id="facebook"> Facebook</a> |
				
				<a href="https://www.instagram.com/joao.ferreira9899" target="_blank" id="instagram"> Instagram </a>
				</p>
				
			</footer>
			
		</div>
		
<!--=======================================================================================================================================-->
		
	</body>


</html>
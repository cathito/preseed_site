<?php
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}
	include_once("classes/conexao.php");
?>

<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Simulando </title>
		<link rel="stylesheet" href="_css/estatiSimResolv.css">
		
	</header>
	
	
	<body>
		
		<h2> Veja Sua Desenvoltura </h2>
		
			<?php
				
				$sql = "select simuladosFeitos.id_simuladoLancado, simuladosFeitos.gabaritoUsuario, simuladosFeitos.acertos, simuladosLancados.descrissao
						from simuladosFeitos join simuladosLancados
						on simuladosFeitos.id_simuladoLancado = simuladosLancados.id_simuladoLancado
						where simuladosFeitos.id_usuario = ".$_SESSION['id_usuario']." order by id_simuladoFeito desc;";
				
				$resultado_data = mysqli_query($conn, $sql);
				
				if ($resultado_data) {
					
					While($dados = mysqli_fetch_assoc($resultado_data)){
						
						echo"
						<fieldset id='maior'> <legend> &nbsp; Simulado (".$dados['id_simuladoLancado'].") -- ".$dados['descrissao']." &nbsp;</legend>
						
							<table id='tabelaspec'>
								<tr> <td class='ce'> Simulado Composto por: </td> <td class='cd'> ".strlen($dados['gabaritoUsuario'])." questoes </td></tr>
								<tr> <td class='ce'> Das Quais Foram marcadas corretamente: </td> <td class='cd'> ".$dados['acertos']." questoes </td></tr>
							</table>
						
						</fieldset>";
					
					}
					
				}else {
					echo "<script language='javascript' type='text/javascript'>
							alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 01');
							window.close();
						</script>";
				}
		
				
			?>
		
	</body>
	
	
</html>
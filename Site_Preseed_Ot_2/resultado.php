<?php
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}
	include_once("classes/conexao.php");
	
/*============================================================*/	
	
	$idSimulado = htmlspecialchars(addslashes($_POST['idSimulado']));
	$numeroQuestoes = htmlspecialchars(addslashes($_POST['contador']));
	
  /*--------------------------------------------------*/
  
	$gabaritoOficial;
	$gabaritoEstudante;
	$stringGabaritoUsuario="";
	$acertos=0;
	
/*===============================================================================================================*/	

	function preenherGabaritoEstudante(){
		global $gabaritoEstudante;
		global $stringGabaritoUsuario;
		global $numeroQuestoes;
		
		for($i=0; $i<$numeroQuestoes; $i++){
			$gabaritoEstudante[$i]= htmlspecialchars(addslashes($_POST['Q'.($i+1)]));
			$stringGabaritoUsuario = $stringGabaritoUsuario."".$gabaritoEstudante[$i];
		}
		return true;
	}
	
/*===============================================================================================================*/	

	function preenherGabaritoOficial(){
		global $conn;
		global $gabaritoOficial;
		global $idSimulado;
		
		$sql = "select alternativaCorreta from questoesLancadas where id_simulado = '".$idSimulado."' order by id_questao asc;";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			$i=0;
			While($row_usuario = mysqli_fetch_assoc($resultado)){
				$gabaritoOficial[$i] = $row_usuario['alternativaCorreta'];
				$i++;
			}
			return true;
		}else {
			echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 02');
					window.history.back();
				</script>";
		}
		return false;
	}
	
/*===============================================================================================================*/
	
	function contabilizarAcertos(){
		global $gabaritoOficial;
		global $gabaritoEstudante;
		global $numeroQuestoes;
		global $acertos;
		
		for($i=0; $i<$numeroQuestoes; $i++){
			
			/*echo "Questao ".($i+1)." alternativa Estudante - ".$gabaritoEstudante[$i]." alternativa oficial - ".$gabaritoOficial[$i];*/
			
			if($gabaritoOficial[$i] == $gabaritoEstudante[$i]){
				/*echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;----- Correta! -----<br>";*/
				$acertos++;
			}/*else{
				if($gabaritoEstudante[$i]=="-"){
					echo "&nbsp;&nbsp;&nbsp;&nbsp; Não Respondida! <br>";
				}else{
					echo "&nbsp;&nbsp;&nbsp;&nbsp; ---- Incorreta! ----<br>";
				}
			}*/
			
		}
		return true;
	}
	
/*===============================================================================================================*/

	function simuladoJaRealizado(){
		global $conn;
		global $idSimulado;
		
		$sql = "select id_simuladoFeito from simuladosFeitos where id_usuario = ".$_SESSION['id_usuario']." and id_simuladoLancado=".$idSimulado.";";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			if( ($dados = mysqli_fetch_array($resultado)) != null){
				return true;
			}
		}else {
			echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 01');
				window.history.back();
			</script>";
		}
		return false;
	}
	
/*===============================================================================================================*/		

	function gravarResultado(){
		global $conn;
		global $idSimulado;
		global $stringGabaritoUsuario;
		global $acertos;
		
		
		$sql = "insert into simuladosFeitos values (default,".$_SESSION['id_usuario'].", ".$idSimulado.", '".$stringGabaritoUsuario."', '".$acertos."');";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			echo "<script language='javascript' type='text/javascript'>
					alert('Simulado Cadastrado Com Sucesso!');
				</script>";
			return true;
		}else {
			echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 03');
					window.history.back();
				</script>";
		}
		return false;
	}
	
/*===============================================================================================================*/	
	
?>


<!--============================================================================================================================-->
<!--============================================================================================================================-->


<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Simulando </title>
		<link rel = "stylesheet" href = "_css/resultadoSimulado.css"/>
		<script>
			function fechar(){
				window.close();
			}
			
		</script>
	</header>
	
	<body>
		<?php
			global $stringGabaritoUsuario;
			global $acertos;
			
			if( preenherGabaritoEstudante() ){
				if( preenherGabaritoOficial() ){
					if(contabilizarAcertos()){
						if( !simuladoJaRealizado() ){
							gravarResultado();
						}else{
							echo "<script language='javascript' type='text/javascript'>
									alert('Atenção! \\nA Nota do Referido Simulado Não Sera Computada! \\nVisto Que a Mesma ja Foi Computada Anteriormente.');
								</script>";
						}
					}
				}
			}
			
		?>
		
<!--==============================================================================================-->
		
		<div>
			
			<h2> Parabens! </h2>
			
			<p> Voce Acertou <?php global $acertos;  echo $acertos; ?> questoes </p>
			
			
			
		</div>
		
	</body>
	
	
</html>





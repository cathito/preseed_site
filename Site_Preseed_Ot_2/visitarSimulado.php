
<?php
	
	session_start();
	if(!isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}
	include_once("classes/conexao.php");
	
	$id_simulado =  addslashes($_POST['id_simuladoLancado']);
?>

<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Simulando </title>
		<link rel="stylesheet" href="_css/questaoLancada.css"> 
		
	</header>
	
	
	<body>
	
		<div>
			<?php
				
				$sql = "select * from questoesLancadas where id_simulado = '".$id_simulado."';";
				$resultado_questao = mysqli_query($conn, $sql);
				
				if ($resultado_questao) {
					$contador=0;
					
					echo "<fieldset id='maior'> <legend> &nbsp; Simulado (".$id_simulado.") &nbsp;</legend>";
					
						While($row_usuario = mysqli_fetch_assoc($resultado_questao)){
							++$contador;
							
							echo "<fieldset id='questao'> <legend>&nbsp; Questao (".$contador.") &nbsp;</legend>";
							
							echo "<p id='informativo'>  <strong> -- Informativo -- </strong> <br/>
													   - <strong> Area Do Conheciento: </strong>".$row_usuario['areaDoConheciento']." -<br/>
													   - <strong> Nivel de Dificuldade: </strong> ".$row_usuario['nivelDificuldade']." - //
													   - <strong> Alternativa Correta: </strong> ".$row_usuario['alternativaCorreta']." -
							</p>";
							
							echo " <p>".$row_usuario['pergunta']." </p>";
							echo " <label> (A) - ".$row_usuario['respostaA']." </label><br/>";
							echo " <label> (B) - ".$row_usuario['respostaB']." </label><br/>";
							echo " <label> (C) - ".$row_usuario['respostaC']." </label><br/>";
							echo " <label> (D) - ".$row_usuario['respostaD']." </label><br/>";
							echo " <label> (E) - ".$row_usuario['respostaE']." </label><br/>";
							echo " </fieldset>";
						}
					echo "</fieldset>";
						
				}else {
					echo "<script language='javascript' type='text/javascript'>
							alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 01');
							window.location.href='../login.php';
						</script>";
				}
				
			?>
			
			
		</div>
		
	</body>
	
	
</html>
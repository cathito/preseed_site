<?php

session_start();
if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
	header("location: login.php");
	exit;
}

/*=============================================================================*/

?>

<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Simulando Edit </title>
		<link rel = "stylesheet" href = "_css/cabecalhoRodape.css"/>
		<link rel="stylesheet" href="_css/t_login.css"/>
		
	</header>
	
	
	<body>
	
		<header id = "cabecalho">
			<img id="icone" src = "_imagens/logo2.png"/>
		</header>
		
<!--=======================================================================================================================================-->
		
		<img id="inicio" src = "_imagens/login.png"/>
		
		
		<div id="corpo-form">
			<form method="POST" action="classes/upDadosUsuario.php">
				<h1> Editar Perfil </h1>
				<input type="text" placeholder="Nome Completo" name="nome" maxlength="40"/>
				<input type="email" placeholder="Email" name="email" maxlength="40"/>
				<select name="polo" id="polo" >
					<option value=""> Selecionar Polo</option>
					<option value="Simão Dias" > Polo Simão Dias </option>
					<option value="Outros" > Outros </option>
				</select> 
				
				<br/>
				<input type="password" placeholder="Senha Atual" name="senha" maxlength="12"/>
				
				<input type="submit" value="Finalizar Edição" id="finalizar" name="finalizar"/>
			</form>
		</div>
		
	</body>
	
	
</html>
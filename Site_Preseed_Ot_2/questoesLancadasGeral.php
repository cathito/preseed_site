
<?php
	
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}
	include_once("classes/conexao.php");
?>

<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Simulando </title>
		<link rel="stylesheet" href="_css/questaoLancada.css"> 
		
	</header>
	
	
	<body>
	
		<div>
			<?php
			
				$sql = "select DISTINCT dataDaPublicacao from questoesLancadas order by dataDaPublicacao desc;";
				$resultado_data = mysqli_query($conn, $sql);
				
				if ($resultado_data) {
					
					While($row_data = mysqli_fetch_assoc($resultado_data)){
					
						$sql = "select questoesLancadas.*, usuario.nome, usuario.polo
								from questoesLancadas join usuario
								on questoesLancadas.id_usuario = usuario.id_usuario
								where dataDaPublicacao = '".$row_data['dataDaPublicacao']."';";
						
						$resultado_questao = mysqli_query($conn, $sql);
							
						if ($resultado_questao) {
							$contador=0;
							
							echo "<fieldset id='maior'> <legend> &nbsp; Questoes lancadas em ".$row_data['dataDaPublicacao']." &nbsp;</legend>";
							
								While($row_usuario = mysqli_fetch_assoc($resultado_questao)){
									++$contador;
									
									echo "<fieldset id='questao'> <legend>&nbsp; Questao (".$contador.") &nbsp;</legend>";
									
									echo "<p id='informativo'>  <strong> Membro: </strong>".$row_usuario['nome']." - Polo ".$row_usuario['polo']." <br/>
									
															   <br/> <strong> -- Informativo -- </strong> <br/>
															   - <strong> Area Do Conheciento: </strong>".$row_usuario['areaDoConheciento']." -<br/>
															   - <strong> Nivel de Dificuldade: </strong> ".$row_usuario['nivelDificuldade']." - //
															   - <strong> Alternativa Correta: </strong> ".$row_usuario['alternativaCorreta']." -
									</p>";
									
									echo " <p>".$row_usuario['pergunta']." </p>";
									echo " <label> (A) - ".$row_usuario['respostaA']." </label><br/>";
									echo " <label> (B) - ".$row_usuario['respostaB']." </label><br/>";
									echo " <label> (C) - ".$row_usuario['respostaC']." </label><br/>";
									echo " <label> (D) - ".$row_usuario['respostaD']." </label><br/>";
									echo " <label> (E) - ".$row_usuario['respostaE']." </label><br/>";
									echo " </fieldset>";
								}
							echo "</fieldset>";
							
						}else {
							echo "<script language='javascript' type='text/javascript'>
									alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 02');
									window.close();
								</script>";
						}
					
					}
					
				}else {
					echo "<script language='javascript' type='text/javascript'>
							alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 01');
							window.close();
						</script>";
				}
				
			?>
			
			
		</div>
		
	</body>
	
	
</html>
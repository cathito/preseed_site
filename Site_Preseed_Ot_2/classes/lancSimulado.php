<?php

	session_start();
	if(!isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}

	include_once("conexao.php");
	
/*==============================================================================*/

$areaConhecimento = addslashes($_POST['areaConhecimento']);
$alternativaCorreta = addslashes($_POST['alternativaCorreta']);
$nivelDificuldade = addslashes($_POST['nivelDificuldade']);

$dataDaPublicacao = addslashes($_POST['data']);

$pergunta = addslashes($_POST['pergunta']);
$respostaA = addslashes($_POST['respostaA']);
$respostaB = addslashes($_POST['respostaB']);
$respostaC = addslashes($_POST['respostaC']);
$respostaD = addslashes($_POST['respostaD']);
$respostaE = addslashes($_POST['respostaE']);

$gravarQuestao = addslashes($_POST['gravarQuestao']);

/*==============================================================================*/


if (!$conn) {
	echo "<script language='javascript' type='text/javascript'>
			alert('Falha Na conexao Com o Banco de Dados!\nPedimos Desculpas Pelo Transtorno!');
			window.location.href='../cadastrar.php';
		</script>";
	die();
}

/*==============================================================================*/

if(isset($gravarQuestao)) {
	
	if( ($areaConhecimento == "" || $areaConhecimento == null) ||
		($alternativaCorreta == "" || $alternativaCorreta == null) || 
		($nivelDificuldade == "" || $nivelDificuldade == null) ){
		echo "<script language='javascript' type='text/javascript'>
				alert('Todos os Informativos Deverão Ser Preenchidos!');
				window.location.href='../lancarQuestoes.php';
			</script>";
			
			
	}elseif( ($pergunta == "" || $pergunta == null) || 
			($respostaA == "" || $respostaA == null) ||
			($respostaB == "" || $respostaB == null) ||
			($respostaC == "" || $respostaC == null) ||
			($respostaD == "" || $respostaD == null) ||
			($respostaE == "" || $respostaE == null) ){
		
		echo "<script language='javascript' type='text/javascript'>
				alert('Todos as Informações  Pertecentes a Questão Deverão Ser Preenchidas!');
				window.location.href='../lancarQuestoes.php';
			</script>";
			
	}else{
		
		$sql = "insert into questoeslancadas values (default, '".$_SESSION['id_usuario']."', 0,'".$dataDaPublicacao."', '".$areaConhecimento."', '".$alternativaCorreta."', '".$nivelDificuldade."', '".$pergunta."', '".$respostaA."', '".$respostaB."', '".$respostaC."', '".$respostaD."', '".$respostaE."');";
		
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			echo"<script language='javascript' type='text/javascript'> 
					alert('Questão Cadastrada com Sucesso!');
					window.location.href='../lancarQuestoes.php';
				</script>";
		}else {
			echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado!\nPedimos Desculpas Pelo Transtorno!');
					window.location.href='../lancarQuestoes.php';
				</script>";
		}
		
	}
	
	
}
/*==============================================================================*/

?>
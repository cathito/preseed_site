<?php
	session_start();
	if(!isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}
	include_once("conexao.php");
	
/*============================================================*/	
	
	$idSimulado = addslashes($_POST['idSimulado']);
	$numeroQuestoes = addslashes($_POST['contador']);
	
  /*--------------------------------------------------*/
  
	$gabaritoEstudante;
	$gabaritoOficial;
	$stringGabaritoUsuario="";
	$acertos=0;
	
	$simuladoJaFoiFeito = false;
/*===============================================================================================================*/	
	
	
	$sql = "select * from simuladosFeitos where id_usuario = '".$_SESSION['id_usuario']."' and id_simuladoLancado='".$idSimulado."';";
	$resultado = mysqli_query($conn, $sql);
	
	if ($resultado) {
		if( ($dados = mysqli_fetch_array($resultado)) != null){
			$simuladoJaFoiFeito = true;
		}
	}else {
		echo "<script language='javascript' type='text/javascript'>
			alert('Comportamento Inesperado!\nPedimos Desculpas Pelo Transtorno!');
			window.location.href='../resolverSimulado.php';
		</script>";
	}
	
/*===============================================================================================================*/	
	
	/* ---------- preencger gabaritoEstudante ---------- */
		
	for($i=1; $i<=$numeroQuestoes; $i++){
		$gabaritoEstudante[$i]= addslashes($_POST['Q'.$i]);
		$stringGabaritoUsuario = $stringGabaritoUsuario."".$gabaritoEstudante[$i];
	}
	
		/* ----------- preencher gabaritoOficial ----------- */
	
	$sql = "select alternativaCorreta from questoeslancadas where id_simulado = '".$idSimulado."' order by id_questao asc;";
	$resultado = mysqli_query($conn, $sql);
	
	$i=1;
	if ($resultado) {
		
		While($row_usuario = mysqli_fetch_assoc($resultado)){
			$gabaritoOficial[$i] = $row_usuario['alternativaCorreta'];
			$i++;
		}
		
	}else {
		echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! Pedimos Desculpas Pelo Transtorno!');
				window.location.href='../resolverSimulado.php';
			</script>";
	}
/*===============================================================================================================*/		
	
	if(!$simuladoJaFoiFeito){
		
		for($i=1; $i<=$numeroQuestoes; $i++){
			if(strcmp($gabaritoOficial[$i] , $gabaritoEstudante[$i])==0){
				$acertos++;
			}
		}
	 /*-----------------------------------------------------------------------------------------*/
		
				/*--------------- gravar no Banco ---------------*/

		$sql = "insert into simuladosFeitos values (default,".$_SESSION['id_usuario'].", ".$idSimulado.", '".$stringGabaritoUsuario."', '".$acertos."');";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			echo "<script language='javascript' type='text/javascript'>
					alert('Simulado Cadastrado Com Sucesso!');
				</script>";
		}else {
			echo "<script language='javascript' type='text/javascript'>
					alert('Comportamento Inesperado! Pedimos Desculpas Pelo Transtorno!');
					window.location.href='../resolverSimulado.php';
				</script>";
		}
		
	}else{
		echo "<script language='javascript' type='text/javascript'>
				alert('Atenção! A Nota do referido Simulado Não Sera Computada, Visto Que o Mesmo ja Foi computado Anteriormente.');
			</script>";
	}
	
?>


<!--============================================================================================================================-->
<!--============================================================================================================================-->


<html lang = "pt-br">

	<header>
		<meta charset="utf-8">
		<title> Menu do Usuario </title>
		<link rel = "stylesheet" href = "../_css/resultadoSimulado.css"/>
		<script>
			function fechar(){
				window.close();
			}
			
		</script>
	</header>
	
	<body>
		<div>
			<h2> Seu Gabarito Foi! </h2>
			
			<p>
				<?php
					for($i=1; $i<=$numeroQuestoes; $i++){
						echo "Questao ".($i)." alternativa - ".$gabaritoEstudante[$i];
						if(strcmp($gabaritoOficial[$i] , $gabaritoEstudante[$i])==0){
							echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;----- Correta! -----<br>";
						}else{
							if($gabaritoEstudante[$i]=="-"){
								echo "&nbsp;&nbsp;&nbsp;&nbsp; Não Respondida! <br>";
							}else{
								echo "&nbsp;&nbsp;&nbsp;&nbsp; ---- Incorreta! ----<br>";
							}
						}
					}
					echo "<br>Voce Acertou ".$acertos." questoes <br>";
				?>
			</p>
			
			
			<input type="submit" value="Fechar" onclick="fechar()"> 
			
			
		</div>
	</body>
	
	
</html>





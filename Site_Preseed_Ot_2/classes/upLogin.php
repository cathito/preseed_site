<?php
	session_start();
	if(!isset($_SESSION['id_login']) || !isset($_SESSION['id_usuario'])){
		header("location: login.php");
		exit;
	}

	include_once("conexao.php");
	
	$novoLogin = htmlspecialchars(addslashes($_POST['novoLogin']));
	
	$senha = htmlspecialchars(addslashes($_POST['senha']));
	
	$cadastrar = htmlspecialchars(addslashes($_POST['finalizar']));
	
	
	
	if(isset($cadastrar)) {
		if(camposPreenchidos($novoLogin, $senha)){
			if(!loginEncontrado($novoLogin)){
				if(usuarioValido($senha)){
					atualizar($novoLogin);
				}
			}
		}
	}
	
/*====================================================================================================*/
/*====================================================================================================*/

	function camposPreenchidos($novoLogin, $senha){ 
	
		if( ($novoLogin == "" || $novoLogin == null) || ($senha == "" || $senha == null) ){
			echo "<script >
						alert('Todos os Campos Devem Ser Preenchidos!');
						window.history.back();
					</script>";
			return false;
		}
		return true;
	}
/*----------------------------------------------------------------------------------*/
	
	function loginEncontrado($novoLogin){ 
		global $conn;
		
		$sql = "select * from logonn where login = '".$novoLogin."';";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			if(($dados = mysqli_fetch_array($resultado)) == null){
				return false;					
			}
		}else{
			echo "<script >
					alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (01)');
					window.history.back();
				</script>";
		}
		
		echo "<script >
				alert('Login Invalido!');
				window.history.back();
			</script>";
		
		return true;
	}
	
/*----------------------------------------------------------------------------------*/
	
	function usuarioValido($senha){
		global $conn;
		
		$sql = "select id_login from logonn where id_login = '".$_SESSION['id_login']."' and senha = '".$senha."';";
		$resultado = mysqli_query($conn, $sql);
		
		if ($resultado) {
			if(($dados = mysqli_fetch_array($resultado)) != null){
				return true;
			}
		}else {
			echo "<script language='javascript' type='text/javascript'>
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! 01');
				window.history.back();
			</script>";
		}
		return false;
	}
/*----------------------------------------------------------------------------------*/
	
	function atualizar($novoLogin){
		global $conn;
		
		$sql = "UPDATE logonn SET login = '".$novoLogin."' WHERE id_login = ".$_SESSION['id_login'].";";
		$resultado = mysqli_query($conn, $sql);

		if ($resultado) {
			echo "<script language='javascript' type='text/javascript'>
					alert('Login Editado com Sucesso!');
					window.history.go(-2);
				</script>";
		}else {
			echo "<script >
				alert('Comportamento Inesperado! \\nPedimos Desculpas Pelo Transtorno! (02)');
				window.history.back();
			</script>";
		}
	}
/*----------------------------------------------------------------------------------*/
	
?>